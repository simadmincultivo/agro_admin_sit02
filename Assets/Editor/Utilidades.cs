﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System.Diagnostics;
using System.Timers;
using System.Threading;
using System;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

//using NSTranslateSystem;
using NSTraduccionIdiomas;

public class Utilidades
{
    //VARAIBLES

    //----------------------
    //---APP NAME
    const string COMPANY = "CloudLabs";

    //SPA
    static string PRODUCT_SPA = "Cloudlabs - Simulador de costos variables de produccción en el manejo de un cultivo de tomate";
    static string PRODUCT_SPA_OSX = "Cloudlabs - Administracion cultivos";
    static string PRODUCT_ENG = "Cloudlabs - Geometry 3D";
    static string PRODUCT_POR = "CloudLabs - Simulador de fotossintese";


    //----------------------
    //---identifier
    static string SPA_ID = "com.cloudlabs.semilleros.produccion.v4";
    static string ENG_ID = "com.cloudlabs.agro.admincultivos.eng";
    static string POR_ID = "com.cloudlabs.agro.admincultivos.por";


    //----------------------
    //---LANG

    //DiccionarioIdiomas.Instance.SetIdiom(IdiomsList.Spanish);
    static IdiomsList Lang_SPA = IdiomsList.Spanish;
    static IdiomsList Lang_ENG = IdiomsList.English;

    //---BIL LANG
    /*static menu.ConfigureLanguageList SPA_ENG = menu.ConfigureLanguageList.Spanish_English;
    static menu.ConfigureLanguageList POR_ENG = menu.ConfigureLanguageList.English_Portuguese;*/
    
 

    //----
    static bool SECURITY;

    //keyword
    static string simKeyword = "agro_admin02";
    static string simKeyword_U = "agro_admin02_u";
    static string simKeyword_K12 = "agro_admin02_k";
    static string std_folder = @"DEPLOY\Release\STD\";
    static string mono_folder = @"DEPLOY\Release\MONO\";

    [MenuItem("IE/Seguridad")]
    public static void Seguridad()
    {

        /*GameObject.FindObjectOfType<menu>().SecurityModule = !GameObject.FindObjectOfType<menu>().SecurityModule;
        SECURITY = GameObject.FindObjectOfType<menu>().SecurityModule;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        if (GameObject.FindObjectOfType<menu>().SecurityModule)
        {
            EditorUtility.DisplayDialog("SEGURIDAD", "SEGURIDAD ACTIVADA", "ok");
        }
        else
        {
            EditorUtility.DisplayDialog("SEGURIDAD", "NOT", "ok");
        }*/

        segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isSecurityOn = !segController.soSecurityConfiguration.isSecurityOn;

        if (segController.soSecurityConfiguration.isSecurityOn)
        {
            EditorUtility.DisplayDialog("SEGURIDAD", "SEGURIDAD ACTIVADA", "ok");
        }
        else
        {
            EditorUtility.DisplayDialog("SEGURIDAD", "NOT", "ok");
        }

    }

    static NSSeguridad.Seguridad segController;
    static DiccionarioIdiomas IdioDict;
    static NSInitSimulator.InitSimulator initCntroller;


   [MenuItem("IE/Build")]
    static void Build()
    {
        //PlayerPrefs.DeleteAll();


        /*GameObject.FindObjectOfType<menu>().SecurityModule = true;
        SECURITY = GameObject.FindObjectOfType<menu>().SecurityModule;*/

        segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isSecurityOn = true;
        SECURITY = segController.soSecurityConfiguration.isSecurityOn;

        //--language
        initCntroller = GameObject.Find("InitSimulador").GetComponent<NSInitSimulator.InitSimulator>();
        initCntroller.simulatorIsBilingual.Value = false;
        
        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_SPA);

        segController.soSecurityConfiguration.isModeClassroom = false;



        //GameObject.FindObjectOfType<menu>().LanguageEnabled = false;
        //GameObject.FindObjectOfType<BaseSimulator>().Aula = false;

        //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;

        //webGL <--
        sim_SPA_webGL("k12");
        /*sim_ENG_webGL("k12");
        sim_POR_webGL("k12");
        sim_B_webGL("k12");*/

        //sim_SPA_webGL_free("k12");

        //sim_SPA_webGL_AULA();
        sim_SPA_webGL_AULA_nuevaurl();
        /*sim_ENG_webGL_AULA();*/

        //PC -->----------------
        //--
        sim_SPA_PC("k12");
        //sim_SPA_PC_mono("k12");/**/
        /*sim_ENG_PC("k12");
        sim_ENG_PC_mono("k12");
        sim_POR_PC("k12");
        sim_POR_PC_mono("k12");
        sim_B_PC("k12");
        sim_B_PC_mono("k12");*/
        //----------------------

        //OSX <--
        

        sim_SPA_OSX("k");
        //sim_SPA_OSX_mono("k");/**/
        /*sim_ENG_OSX("k");
        sim_ENG_OSX_mono("k");*/
        //----------------------

        //APK <--


        sim_SPA_APK("k");
        //sim_SPA_APK_mono("k");/**/
        /*sim_ENG_APK("k");
        sim_ENG_APK_mono("k");*/
        /*sim_POR_APK("k");
        sim_POR_APK_mono("k");
        sim_B_APK("k");
        sim_B_APK_mono("k");*/
        //----------------------

    }



    static BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
    static void groupScenes()
    {


        buildPlayerOptionsScene.scenes = new[] { "Assets/Scenes/Login.unity",
            "Assets/Scenes/Practica2.unity"};
    }

    //Exportaciones
    //PC - 
    static void sim_SPA_PC(string complexType)
    {

        //-------------------------------------------------------------
        //CMD
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = SECURITY;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_U + "_spa_std/" + simKeyword_U + "_spa_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_K12 + "_spa_std/" + simKeyword_K12 + "_spa_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_k12.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);
        }

        //----------------------------------------------------------------------------------------
    }
    static void sim_SPA_PC_mono(string complexType)
    {

        //-------------------------------------------------------------
        //CMD
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = true;
        segController.soSecurityConfiguration.isSecurityOn = SECURITY;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();




        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            //BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, @"DEPLOY\MONO\autom_mesaplc_graf_spa_std\autom_mesaplc_graf_spa_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);
            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword_U + "_spa_mono/" + simKeyword_U + "_spa_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_mono_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            //BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, @"DEPLOY\MONO\autom_mesaplc_graf_spa_std\autom_mesaplc_graf_spa_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);
            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword_K12 + "_spa_mono/" + simKeyword_K12 + "_spa_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_mono_k12.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);
        }

        //----------------------------------------------------------------------------------------
    }

    //webGL -  free
    static void sim_SPA_webGL_free(string complexType)
    {
        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;


        string path = std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        /*if (complexType == "U")
        {
            GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti_u/" + simKeyword + "_spa_lti_u", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
            AssetDatabase.Refresh();

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {*/
        //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti_free.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/


    }

    //webGL - 
    static void sim_SPA_webGL(string complexType)
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = true;

        string path = std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        /*if (complexType == "U")
        {
            GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti_u/" + simKeyword + "_spa_lti_u", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
            AssetDatabase.Refresh();

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {*/
        //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/


    }
    static void sim_ENG_webGL(string complexType)
    {

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_ENG);

        //--idioma option
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = SECURITY;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isLtiActive = true;


        string path = std_folder + simKeyword + "_eng_lti/" + simKeyword + "_eng_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_lti_u/" + simKeyword + "_eng_lti_u", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_weblti_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);/**/

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_lti/" + simKeyword + "_eng_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_weblti.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);/**/
        }/**/

    }
    static void sim_POR_webGL(string complexType)
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        /*Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();
        if (complexType == "U")
        {
            GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
        }
        else
        {
            GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
        }*/

        /*GameObject.FindObjectOfType<BaseSimulator>().SecurityModule = false;
        GameObject.FindObjectOfType<BaseSimulator>().MonoUser = false;

        GameObject.FindObjectOfType<BaseSimulator>().Language = Lang_POR;
        GameObject.FindObjectOfType<BaseSimulator>().LanguageConf = POR_ENG;
        GameObject.FindObjectOfType<BaseSimulator>().LanguageEnabled = false;*/


        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_lti_u/" + simKeyword + "_por_lti_u", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_std_weblti_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);/**/

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_lti/" + simKeyword + "_por_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_std_weblti.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);/**/
        }/**/

    }
    static void sim_B_webGL(string complexType)
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        /*Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();
        if (complexType == "U")
        {
            GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
        }
        else
        {
            GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
        }*/

        /*GameObject.FindObjectOfType<BaseSimulator>().SecurityModule = false;
        GameObject.FindObjectOfType<BaseSimulator>().MonoUser = false;

        GameObject.FindObjectOfType<BaseSimulator>().Language = Lang_ENG;
        GameObject.FindObjectOfType<BaseSimulator>().LanguageConf = SPA_ENG;
        GameObject.FindObjectOfType<BaseSimulator>().LanguageEnabled = true;*/


        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_bspaeng_lti_u/" + simKeyword + "_bspaeng_lti_u", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BSPAENG_std_weblti_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);/**/

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_bspaeng_lti/" + simKeyword + "_bspaeng_lti", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_BSPAENG_std_weblti.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);/**/
        }/**/

    }
    
    static void sim_SPA_webGL_AULA()
    {
        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = true;
        segController.soSecurityConfiguration.isLtiActive = false;
        segController.soSecurityConfiguration.WebAulaUrl = "http://servercloudlabs.com/validarSimulador/informacionUsuario";


        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_spa_webAULA/" + simKeyword + "_spa_webAULA";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_webAULA/" + simKeyword + "_spa_webAULA", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_webAULA.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }


    static void sim_SPA_webGL_AULA_nuevaurl()
    {
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = true;
        segController.soSecurityConfiguration.isLtiActive = false;
        segController.soSecurityConfiguration.WebAulaUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";


        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_spa_webAULA_nuevaurl/" + simKeyword + "_spa_webAULA_nuevaurl";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_webAULA_nuevaurl/" + simKeyword + "_spa_webAULA_nuevaurl", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_webAula_nuevaurl.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }


    //[MenuItem("IE/sim_ENG_webGL")]
    static void sim_ENG_webGL_AULA()
    {

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_ENG);

        //--idioma option
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();

        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = false;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = true;


        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_eng_webAULA/" + simKeyword + "_eng_webAULA";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_webAULA/" + simKeyword + "_eng_webAULA", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_webAULA.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }


    //osx 
    static void sim_SPA_OSX(string complexType)
    {
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = SECURITY;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;

        /*GameObject.FindObjectOfType<menu>().Language = Lang_SPA;
        GameObject.FindObjectOfType<menu>().LanguageConf = SPA_ENG;
        GameObject.FindObjectOfType<menu>().LanguageEnabled = false;

        GameObject.FindObjectOfType<menu>().MonoUser = false;
        GameObject.FindObjectOfType<menu>().SecurityModule = SECURITY;*/

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();




        //----------------------------------------------------------------------------------------

        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_U + "_spa_s_x/" + PRODUCT_SPA_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_std_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_K12 + "_spa_s_x/" + PRODUCT_SPA_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_std_k12.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);
        }
    }
    static void sim_SPA_OSX_mono(string complexType)
    {
        //GameObject.FindObjectOfType<BaseSimulator>().MonoUser = true;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = true;
        segController.soSecurityConfiguration.isSecurityOn = SECURITY;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;

        /*GameObject.FindObjectOfType<menu>().Language = Lang_SPA;
        GameObject.FindObjectOfType<menu>().LanguageConf = SPA_ENG;
        GameObject.FindObjectOfType<menu>().LanguageEnabled = false;

        GameObject.FindObjectOfType<menu>().MonoUser = true;
        GameObject.FindObjectOfType<menu>().SecurityModule = SECURITY;*/



        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();




        //----------------------------------------------------------------------------------------

        if (complexType == "U")
        {

            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword_U + "_spa_m_x/" + PRODUCT_SPA_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_mono_u.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);

        }
        else
        {

            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword_K12 + "_spa_m_x/" + PRODUCT_SPA_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



            //Empaquetado
            ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_mono_k12.bat");
            startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
            Process.Start(startInfo2);
        }
    }


    //apk
    static void sim_SPA_APK(string complexType)
    {
        //GameObject.FindObjectOfType<BaseSimulator>().MonoUser = false;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, SPA_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = false;
        segController.soSecurityConfiguration.isSecurityOn = SECURITY;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;

        /*GameObject.FindObjectOfType<menu>().Language = Lang_SPA;
        GameObject.FindObjectOfType<menu>().LanguageConf = SPA_ENG;
        GameObject.FindObjectOfType<menu>().LanguageEnabled = false;

        GameObject.FindObjectOfType<menu>().MonoUser = false;
        GameObject.FindObjectOfType<menu>().SecurityModule = SECURITY;*/

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();



        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_U + "_spa_std_u.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword_K12 + "_spa_std_k12.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);

        }
        //----------------------------------------------------------------------------------------
    }
    static void sim_SPA_APK_mono(string complexType)
    {
        //GameObject.FindObjectOfType<BaseSimulator>().MonoUser = true;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, SPA_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;

        //--idioma set
        IdioDict = GameObject.Find("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        IdioDict.SetIdiom(Lang_SPA);

        //--idioma option
        initCntroller.simulatorIsBilingual.Value = false;

        //segController = GameObject.Find("Seguridad").GetComponent<NSSeguridad.Seguridad>();
        segController.soSecurityConfiguration.isModeMonoUser = true;
        segController.soSecurityConfiguration.isSecurityOn = SECURITY;
        segController.soSecurityConfiguration.isConectionActive = true;
        segController.soSecurityConfiguration.isModeClassroom = false;
        segController.soSecurityConfiguration.isLtiActive = false;

        /*GameObject.FindObjectOfType<menu>().Language = Lang_SPA;
        GameObject.FindObjectOfType<menu>().LanguageConf = SPA_ENG;
        GameObject.FindObjectOfType<menu>().LanguageEnabled = false;

        GameObject.FindObjectOfType<menu>().MonoUser = true;
        GameObject.FindObjectOfType<menu>().SecurityModule = SECURITY;*/

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();



        if (complexType == "U")
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.U;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword_U + "_spa_mono_u.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);
        }
        else
        {
            //GameObject.FindObjectOfType<menu>().complexMode = BaseSimulator.ComplexModes.K12;
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
            BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword_K12 + "_spa_mono_k12.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);
        }
        //----------------------------------------------------------------------------------------
    }

}