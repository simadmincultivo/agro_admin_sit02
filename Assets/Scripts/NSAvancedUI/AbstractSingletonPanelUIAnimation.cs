﻿#pragma warning disable 0649
using System.Collections;
using System.Collections.Generic;
using NSScriptableEvent;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace NSAvancedUI
{
    [RequireComponent(typeof(CanvasGroup))]
    public abstract class AbstractSingletonPanelUIAnimation<T> : MonoBehaviour where T : AbstractSingletonPanelUIAnimation<T>
    {
        #region singleton

        protected static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    var tmpObjectsOfType = FindInstanceInScene();

                    if (tmpObjectsOfType != null)
                        instance = tmpObjectsOfType;
                    else
                        instance = (new GameObject(typeof(T).ToString())).AddComponent<T>();
                }

                return instance;
            }
        }

        public static void DestroySingleton()
        {
            Destroy(instance.gameObject);
        }

        public void CreateInstance()
        {
            if (instance.transform.parent)
                Debug.LogWarning("Cant be persistent singleton , the object named : " + instance.name + " has a parent named : " + instance.transform.parent.name);
            else
                DontDestroyOnLoad(instance.gameObject);
        }

        private static T FindInstanceInScene()
        {
            var tmpObjectsInScene = new List<T>();

            foreach (var tmpObjectFinded in Resources.FindObjectsOfTypeAll<T>())
            {
                if (tmpObjectFinded.hideFlags == HideFlags.NotEditable && tmpObjectFinded.hideFlags == HideFlags.HideAndDontSave)
                    continue;

#if UNITY_EDITOR
                if(EditorUtility.IsPersistent(tmpObjectFinded))
                    continue;
#endif
                tmpObjectsInScene.Add(tmpObjectFinded.GetComponent<T>());
            }

            return tmpObjectsInScene.Count > 0 ? tmpObjectsInScene[0] : null;
        }

        #endregion

        #region members

        /// <summary>
        /// Curva de animacion de la escala del panel cuando la ventana se esta mostrando
        /// </summary>
        [Header("Animación panel")] [SerializeField, Tooltip("Curva de animacion de la escala del panel cuando la ventana se esta mostrando")]
        private SOAnimationsCurvePanelUI soAnimationsCurvePanelUI;

        /// <summary>
        /// Para controlar la transparencia del panel
        /// </summary>
        private CanvasGroup canvasGroup;

        private IEnumerator couShowPanel;

        private IEnumerator couHiddePanel;

        [Header("Image background panel")] [SerializeField]
        private SOImageBackgroundPanelConfiguration soImageBackgroundPanelConfiguration;

        private GameObject goBackgroundImage;

        #endregion

        #region events

        /// <summary>
        /// Ejecutar evento cuando el panel aparece?
        /// </summary>
        [Header("Eventos"), Space(10)] [SerializeField, Tooltip("Ejecutar evento cuando el panel aparece?")]
        private bool callEventOnShow;

        /// <summary>
        /// Se ejecuta cuando el panel completa su aparicion
        /// </summary>
        [SerializeField, Tooltip("Se ejecuta cuando el panel completa su aparicion")]
        private ScriptableEvent seShowPanel;

        /// <summary>
        /// Ejecutar evento cuando el panel se oculta?
        /// </summary>
        [SerializeField, Tooltip("Ejecutar evento cuando el panel se oculta?")]
        private bool callEventHidde;

        /// <summary>
        /// Se ejecuta cuando el panel completa su ocultacion
        /// </summary>
        [SerializeField, Tooltip("Se ejecuta cuando el panel completa su ocultacion")]
        private ScriptableEvent seHiddePanel;

        #endregion

        #region accesores

        public bool CallEventHidde
        {
            set { callEventHidde = value; }
        }

        public bool PanelOpened { get; set; }

        #endregion

        #region public methods

        /// <summary>
        /// Asigna el bool que permite que se pueda ejecutar el evento que notifica que debe ejecutarse el evento de ocultacion
        /// </summary>
        /// <param name="argEjecutarEvento">Ejecutar el evento de ocultacion? </param>
        public void CallEventHiddeMethod(bool argEjecutarEvento)
        {
            callEventHidde = argEjecutarEvento;
        }

        private void HiddePanel()
        {
            ShowPanel(false);
        }

        /// <summary>
        /// Ejecuta la animacion del panel para este aparesca o se oculte
        /// </summary>
        /// <param name="argShowPanel">ShowPanel el panel? o ocultarlo?</param>
        public virtual void ShowPanel(bool argShowPanel = true)
        {
            if (canvasGroup == null)
                canvasGroup = GetComponent<CanvasGroup>();

            if (argShowPanel)
            {
                if (couShowPanel == null)
                {
                    gameObject.SetActive(true);

                    if (soImageBackgroundPanelConfiguration)
                        if (soImageBackgroundPanelConfiguration.ShowBackgroundImage)
                        {
                            goBackgroundImage = new GameObject("ventanaOscura");
                            goBackgroundImage.transform.SetParent(transform.parent);

                            var tmpImage = goBackgroundImage.AddComponent<Image>();
                            var tmpColorBackgroundImage = soImageBackgroundPanelConfiguration.ColorBackgroundImage;
                            tmpImage.color = new Color(tmpColorBackgroundImage[0], tmpColorBackgroundImage[1], tmpColorBackgroundImage[2], 0);
                            tmpImage.raycastTarget = true;

                            var tmpRectTransform = goBackgroundImage.GetComponent<RectTransform>();
                            tmpRectTransform.anchorMax = Vector2.one;
                            tmpRectTransform.anchorMin = Vector2.zero;
                            tmpRectTransform.offsetMax = Vector2.zero;
                            tmpRectTransform.offsetMin = Vector2.zero;
                            tmpRectTransform.localScale = Vector2.one;

                            goBackgroundImage.GetComponent<Transform>().SetSiblingIndex(transform.GetSiblingIndex());

                            if (soImageBackgroundPanelConfiguration.ClickOverBackgroundImageClosePanel)
                            {
                                var tmpButton = goBackgroundImage.AddComponent<Button>();
                                tmpButton.onClick.AddListener(() =>
                                {
                                    if (PanelOpened)
                                        HiddePanel();
                                });
                            }
                        }

                    StopAllCoroutines();
                    couHiddePanel = null;
                    couShowPanel = CouShowPanel();
                    StartCoroutine(couShowPanel);
                }
                else
                    Debug.LogWarning("El panel ya esta ejecutando una animacion para aparecer");
            }
            else
            {
                if (!gameObject.activeSelf)
                    return;

                if (couHiddePanel == null)
                {
                    StopAllCoroutines();
                    couShowPanel = null;
                    couHiddePanel = CouHiddePanel();
                    StartCoroutine(CouHiddePanel());
                }
                else
                    Debug.LogWarning("El panel ya esta ejecutando una animacion para ocultarse");
            }
        }

        #endregion

        #region courutines

        /// <summary>
        /// Courutina que ejecuta la animacion que hace aparecer el panel
        /// </summary>
        private IEnumerator CouShowPanel()
        {
            var tmpAnimationTime = 0f;
            var tmpRectTransform = GetComponent<RectTransform>();
            Image tmpBackgroundImage = null;

            if (goBackgroundImage)
                tmpBackgroundImage = goBackgroundImage.GetComponent<Image>();

            var tmpAnimationCurveScale = soAnimationsCurvePanelUI._animationCurveEscalaAparecer;
            var tmpAnimationCurveAlpha = soAnimationsCurvePanelUI._animationCurveTransparenciaAparecer;
            var tmpMaxTimeAnimationCurveScale = tmpAnimationCurveScale.keys[tmpAnimationCurveScale.keys.Length - 1].time;
            var tmpMaxTimeAnimationCurveAlpha = tmpAnimationCurveAlpha.keys[tmpAnimationCurveAlpha.keys.Length - 1].time;
            var tmpTimeForShow = soAnimationsCurvePanelUI.TiempoAparicion;

            while (tmpAnimationTime <= tmpTimeForShow)
            {
                tmpAnimationTime += Time.deltaTime;
                tmpRectTransform.localScale = Vector3.one * tmpAnimationCurveScale.Evaluate((tmpAnimationTime * tmpMaxTimeAnimationCurveScale) / tmpTimeForShow);
                canvasGroup.alpha = tmpAnimationCurveAlpha.Evaluate((tmpAnimationTime * tmpMaxTimeAnimationCurveAlpha) / tmpTimeForShow);

                if (tmpBackgroundImage)
                {
                    var tmpColorBackgroundImage = soImageBackgroundPanelConfiguration.ColorBackgroundImage;
                    tmpBackgroundImage.color = new Color(tmpColorBackgroundImage[0], tmpColorBackgroundImage[1], tmpColorBackgroundImage[2], tmpAnimationCurveAlpha.Evaluate((tmpAnimationTime * tmpMaxTimeAnimationCurveAlpha) / tmpTimeForShow) * tmpColorBackgroundImage[3]);
                }

                yield return null;
            }

            if (callEventOnShow)
                seShowPanel.ExecuteEvent();

            couShowPanel = null;
            PanelOpened = true;
        }

        /// <summary>
        /// Courutina que ejecuta la animacion que hace ocultar el panel
        /// </summary>
        private IEnumerator CouHiddePanel()
        {
            var tmpTiempoAnimacion = 0f;
            var tmpRectTransform = GetComponent<RectTransform>();
            Image tmpImagenFondo = null;

            if (goBackgroundImage)
                tmpImagenFondo = goBackgroundImage.GetComponent<Image>();

            var tmpAnimationCurveScale = soAnimationsCurvePanelUI._animationCurveEscalaOcultar;
            var tmpAnimationCurveAlpha = soAnimationsCurvePanelUI._animationCurveTransparenciaOcultar;
            var tmpMaxTimeAnimationCurveScale = tmpAnimationCurveScale.keys[tmpAnimationCurveScale.keys.Length - 1].time;
            var tmpMaxTimeAnimationCurveAlpha = tmpAnimationCurveAlpha.keys[tmpAnimationCurveAlpha.keys.Length - 1].time;
            var tmpTimeForShow = soAnimationsCurvePanelUI.TiempoOcultacion;

            while (tmpTiempoAnimacion <= tmpTimeForShow)
            {
                tmpTiempoAnimacion += Time.deltaTime;
                tmpRectTransform.localScale = Vector3.one * tmpAnimationCurveScale.Evaluate((tmpTiempoAnimacion * tmpMaxTimeAnimationCurveScale) / tmpTimeForShow);
                canvasGroup.alpha = tmpAnimationCurveAlpha.Evaluate((tmpTiempoAnimacion * tmpMaxTimeAnimationCurveAlpha) / tmpTimeForShow);

                if (tmpImagenFondo)
                {
                    var tmpColorBackgroundImage = soImageBackgroundPanelConfiguration.ColorBackgroundImage;
                    tmpImagenFondo.color = new Color(tmpColorBackgroundImage[0], tmpColorBackgroundImage[1], tmpColorBackgroundImage[2], tmpAnimationCurveAlpha.Evaluate((tmpTiempoAnimacion * tmpMaxTimeAnimationCurveAlpha) / tmpTimeForShow) * tmpColorBackgroundImage[3]);
                }

                yield return null;
            }

            if (callEventHidde)
                seHiddePanel.ExecuteEvent();

            couHiddePanel = null;
            PanelOpened = false;
            gameObject.SetActive(false);

            if (soImageBackgroundPanelConfiguration.ShowBackgroundImage)
                if (goBackgroundImage)
                    Destroy(goBackgroundImage);
        }

        #endregion
    }
}