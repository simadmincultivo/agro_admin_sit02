using UnityEngine;

namespace NSScriptableValues
{
    [CreateAssetMenu(fileName = "String", menuName = "NSScriptableValue/String", order = 0)]
    public class ValueString : AbstractScriptableValue<string>
    {
        public static implicit operator string(ValueString argValueA)
        {
            return argValueA.value;
        }
        
        public static implicit operator ValueString(string argValueA)
        {
            var tmpNewValue =  CreateInstance<ValueString>();
            tmpNewValue.Value = argValueA;
            return tmpNewValue;
        }
    }
}