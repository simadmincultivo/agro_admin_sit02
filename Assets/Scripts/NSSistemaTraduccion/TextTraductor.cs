﻿#pragma warning disable 0649
using System;
using NSTraduccionIdiomas;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Clase que se adjunta a cualquier text de la interfaz para que este se pueda traducir
/// </summary>
public class TextTraductor : MonoBehaviour
{
    #region monobehaviour

    private void Start()
    {
        DiccionarioIdiomas.Instance.DLTraducirForNameObj += TraducirGameObjetText;
    }

    private void OnDestroy()
    {
        try
        {
            DiccionarioIdiomas.Instance.DLTraducirForNameObj -= TraducirGameObjetText;
        }
        catch (Exception e)
        {
            Console.WriteLine("No se puede descuscribir el delegado porque el simulador detuvo la ejecucion : " + e);
            throw;
        }
    }

    private void OnEnable()
    {
        TraducirGameObjetText();
    }
    #endregion

    #region private methods

    /// <summary>
    /// identifica el tipo de texto y lo traduse
    /// </summary>
    private void TraducirGameObjetText()
    {
        try
        {
            var tmpTextMeshProUGUI = GetComponent<TextMeshProUGUI>();

            if (tmpTextMeshProUGUI)
            {
                var traduccion = DiccionarioIdiomas.Instance.Traducir(name);
                var textoConFormato = new StringBuilder(traduccion);
                tmpTextMeshProUGUI.SetText(textoConFormato);
                LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent.GetComponent<RectTransform>());
                return;
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Key no existe en el diccionario : "+ e);
        }
        
        try
        {
            var tmpTextDraw = GetComponent<TEXDraw>();

            if (tmpTextDraw)
            {
                var traduccion = DiccionarioIdiomas.Instance.Traducir(name);
                var textoConFormato = new StringBuilder(traduccion);
                tmpTextDraw.text = textoConFormato.ToString();
                LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
                LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent.GetComponent<RectTransform>());
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Key no existe en el diccionario : "+ e);
        }
        
    }
    #endregion
}