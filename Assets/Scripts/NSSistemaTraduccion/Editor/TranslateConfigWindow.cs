﻿using System;
using System.Collections.Generic;
using System.Xml;
using TMPro;
using UnityEditor;
using UnityEngine;

namespace NSTraduccionIdiomas
{
    public class TranslateConfigWindow : EditorWindow
    {
        private static List<TextForTranslate> listTextForTranslates = new List<TextForTranslate>();

        private List<TextMeshProUGUI> listTextMeshProInScene = new List<TextMeshProUGUI>();

        private SOTranslateConfig soTranslateConfig;

        private static bool updateGetAllTextInScene = true;

        private string newIdiomName;

        private Vector2 scrollRectangle;

        [MenuItem("Translate/Configuration")]
        private static TranslateConfigWindow ShowWindow()
        {
            updateGetAllTextInScene = true;
            return GetWindow<TranslateConfigWindow>("Configuration");
        }

        private void OnGUI()
        {
            if (!DrawTraslateConfigSelected())
                return;

            DrawAddNewIdiom();
            DrawButtonUpdateAllTextInScene();
            GetAllTextInScene();
            DrawTextForTraslate();
        }

        private void DrawTextForTraslate()
        {
            var tmpHeightInputs = GUILayout.Height(35);
            var tmpGuiStyleButton = new GUIStyle(GUI.skin.textField);
            tmpGuiStyleButton.alignment = TextAnchor.MiddleCenter;

            var tmpStyleLeft = new GUIStyle();
            tmpStyleLeft.fixedWidth = 600;

            var tmpNewGuiContent = new GUIContent();

            GUI.backgroundColor = new Color(0.69f, 0.69f, 0.69f);

            scrollRectangle = EditorGUILayout.BeginScrollView(scrollRectangle, GUILayout.ExpandHeight(true));
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical(tmpStyleLeft);

            for (int i = 0; i < listTextForTranslates.Count; i++)
            {
                var tmpTextForTranslate = listTextForTranslates[i];
                var myObject = EditorUtility.InstanceIDToObject(tmpTextForTranslate.instanceID);
                listTextMeshProInScene[i] = (TextMeshProUGUI) EditorGUILayout.ObjectField(tmpNewGuiContent, myObject, typeof(TextMeshProUGUI), true, tmpHeightInputs);
            }

            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();

            for (int i = 0; i < listTextForTranslates.Count; i++)
            {
                var tmpTextForTranslate = listTextForTranslates[i];
                
                if (tmpTextForTranslate.textTraductor)
                {
                    var tmpTextMeshProText = tmpTextForTranslate.textMeshProUgui.text;
                    var tmpTagName = tmpTextForTranslate.textMeshProUgui.name;

                    tmpTextMeshProText = soTranslateConfig.GetTranslation(tmpTagName);
                    EditorGUI.BeginChangeCheck();
                    tmpTextMeshProText = GUILayout.TextArea(tmpTextMeshProText, tmpHeightInputs);

                    if (EditorGUI.EndChangeCheck())
                    {
                        soTranslateConfig.EditTranslation(tmpTagName, tmpTextMeshProText);
                        tmpTextForTranslate.textMeshProUgui.SetText(tmpTextMeshProText);
                    }
                }
                else
                {
                    GUI.backgroundColor = Color.green;
                    if (GUILayout.Button("Crear Traduccion", tmpGuiStyleButton, tmpHeightInputs))
                    {
                        tmpTextForTranslate.textTraductor = tmpTextForTranslate.textMeshProUgui.gameObject.AddComponent<TextTraductor>();
                        soTranslateConfig.CreateTranslation(tmpTextForTranslate.textMeshProUgui.name);
                    }

                    GUI.backgroundColor = new Color(0.69f, 0.69f, 0.69f);
                }
            }

            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndScrollView();
        }

        private bool DrawTraslateConfigSelected()
        {
            soTranslateConfig = (SOTranslateConfig) EditorGUILayout.ObjectField("Prueba", soTranslateConfig, typeof(SOTranslateConfig), false);

            if (soTranslateConfig)
                return true;

            GUILayout.Label("Seleccione un SOTranslateConfig valido para continuar");
            return false;
        }

        private void DrawAddNewIdiom()
        {
            EditorGUILayout.BeginVertical();

            newIdiomName = EditorGUILayout.TextField("Nombre nuevo idioma", newIdiomName);

            if (GUILayout.Button("Agregar idioma"))
            {
                if (newIdiomName != "")
                {
                    soTranslateConfig.AddIdiom(newIdiomName);
                    newIdiomName = "";
                    updateGetAllTextInScene = true;
                    EditorUtility.SetDirty(soTranslateConfig);
                }
            }

            var tmpStyleTituloIdioma = new GUIStyle();
            tmpStyleTituloIdioma.fixedWidth = 300;
            tmpStyleTituloIdioma.alignment = TextAnchor.MiddleCenter;

            GUILayout.Box("Lista Idiomas actuales", tmpStyleTituloIdioma, GUILayout.ExpandWidth(true));

            var tmpStyleIdioma = new GUIStyle();
            tmpStyleIdioma.fixedWidth = 100;

            var tmpStyleButton = new GUIStyle();
            tmpStyleButton.fixedWidth = 100;

            foreach (var tmpIdiom in soTranslateConfig.arrayIdiomas)
            {
                GUILayout.BeginHorizontal();

                GUILayout.BeginVertical(tmpStyleIdioma);

                if (tmpIdiom.Equals(soTranslateConfig.idiomaActualSeleccionado))
                    GUI.backgroundColor = Color.green;
                else
                    GUI.backgroundColor = new Color(0.69f, 0.69f, 0.69f);
                
                GUILayout.Box(tmpIdiom, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

                GUILayout.EndVertical();
                
                GUILayout.BeginVertical(tmpStyleButton);

                if (tmpIdiom.Equals(soTranslateConfig.idiomaActualSeleccionado))
                {
                    GUI.backgroundColor = Color.green;
                    GUILayout.Box("Seleccionado", GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
                    GUI.backgroundColor = new Color(0.69f, 0.69f, 0.69f);
                }
                else if (GUILayout.Button("Seleccionar", GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true)))
                {
                    soTranslateConfig.SelectIdiom(tmpIdiom);
                    updateGetAllTextInScene = true;
                    EditorUtility.SetDirty(soTranslateConfig);
                }

                GUILayout.EndVertical();

                GUILayout.BeginVertical(tmpStyleButton);
                
                if (GUILayout.Button("Eliminar",  GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true)))
                {
                    soTranslateConfig.DeleteIdiom(tmpIdiom);
                    updateGetAllTextInScene = true;
                    EditorUtility.SetDirty(soTranslateConfig);
                }

                GUILayout.EndVertical();
                
                GUILayout.EndHorizontal();
            }

            EditorGUILayout.EndVertical();
        }

        private void DrawButtonUpdateAllTextInScene()
        {
            EditorGUILayout.BeginVertical();

            GUI.backgroundColor = Color.green;
            
            if (GUILayout.Button("Recargar textMeshProGUI"))
                updateGetAllTextInScene = true;

            EditorGUILayout.EndVertical();
        }

        [UnityEditor.Callbacks.OnOpenAsset(1)]
        private static bool OnSelectSOTranslateConfigInEditor(int argInstanceID, int argInline)
        {
            var tmpTranslateConfig = EditorUtility.InstanceIDToObject(argInstanceID) as SOTranslateConfig;

            if (tmpTranslateConfig)
            {
                var tmpTranslateConfigWindow = ShowWindow();
                tmpTranslateConfigWindow.soTranslateConfig = tmpTranslateConfig;
                return true;
            }

            return false;
        }

        private void GetAllTextInScene()
        {
            if (updateGetAllTextInScene)
            {
                soTranslateConfig.LoadAllTranslations();
                listTextMeshProInScene.Clear();
                listTextForTranslates.Clear();

                foreach (var tmpObjectFinded in Resources.FindObjectsOfTypeAll<TextMeshProUGUI>())
                {
                    if (tmpObjectFinded.hideFlags == HideFlags.NotEditable || tmpObjectFinded.hideFlags == HideFlags.HideAndDontSave)
                        continue;

                    if (EditorUtility.IsPersistent(tmpObjectFinded.gameObject))
                        continue;

                    listTextMeshProInScene.Add(tmpObjectFinded.GetComponent<TextMeshProUGUI>());
                }

                foreach (var tmpTextMeshProGUI in listTextMeshProInScene)
                    listTextForTranslates.Add(new TextForTranslate(tmpTextMeshProGUI));

                updateGetAllTextInScene = false;
            }
        }
    }
}