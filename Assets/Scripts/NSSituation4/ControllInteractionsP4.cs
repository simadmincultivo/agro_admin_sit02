﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using NSInterfaz;
using NSBoxMessage;
using NSTraduccionIdiomas;
using NSCalificacionSituacion;

public class ControllInteractionsP4 : MonoBehaviour
{
    [Header("Objetos panel de bodega")]
    public TMP_InputField[] fieldsFertilizer;
    public TMP_InputField[] fieldsPlagasEnfermedades;
    public TMP_InputField[] fieldsMalezas;
    public TMP_InputField[] fieldsPodas;
    public TMP_InputField[] fieldsTutorado;

    [Header("Objetos panel de hangar")]
    [SerializeField] private TMP_InputField countAgua;

    [Header("Instancias de scripts")]
    [SerializeField] private ValuesOfProductsManagerP4 myValuesProductsP4;
    [SerializeField] private InstantiateController myInstantiateControll;
    [SerializeField] private ControllArrowBack myControllArrowBack;
    private RegistroProductoController myRegistroProductoController;

    [Header("Objetos panel de jornales invernadero")]
    [SerializeField] private TMP_InputField[] fieldsResorceHuman1;
    
    [Header("Objetos panel de germinadero")]
    [SerializeField] private TMP_InputField fieldResourceHuman2;
    [SerializeField] private TMP_InputField countPlants;

    [Header("Objetos panel dimensiones cultivo")]
    [SerializeField] private TMP_InputField[] fieldDimensionCultivo;
    [Header("Objetos panel distribución cultivo")]
    [SerializeField] private TMP_InputField[] fieldsDistribucionCultivo;
    [Header("Objetos panel fertilizacion y riego")]
    [SerializeField] private TMP_InputField[] fieldsFertilizacionYRiego;
    [Header("Objetos panel poda y tutorado")]
    [SerializeField] private TMP_InputField[] fieldsTutorizadoYPoda;
    [Header("Objetos panel Plagas, enfermedades y malezas")]
    [SerializeField] private TMP_InputField[] fieldsPlagasEnfermedadesMaleza;

    [Header("Camara captura de escenario")]
    [SerializeField] private GameObject cameraCaptureScenary;

    [Header("Boton y paneles para función de felchas")]
    [SerializeField] private Button buttonNoInteractable;
    [SerializeField] private GameObject[] panelsRegisterParametres;

    [Header("Tiempo y texto de espera para desaparecer texto de confirmación")]
    [SerializeField] private float waitTimeConfirmation;

    [Header("Contenedor de objetos de tableta")]
    [SerializeField] private GameObject containerTablet;

    [Header("Referencia de canvas en escena")]
    [SerializeField] private GameObject containerPanels;

    [Header("Contenedor de aumentos")]
    [SerializeField] private GameObject[] containerIncreases;

    private GameObject panelNext;

    void Start()
    {
        PanelInterfazBienvenida2.Instance.ShowPanel();
    }

    public void ButtonAceptarP4(int step)
    {
        string tagNamePackage = DiccionarioIdiomas.Instance.Traducir("NombreBultos");
        string tagNameLiters = DiccionarioIdiomas.Instance.Traducir("NombreLitros");
        string tagNameTool = DiccionarioIdiomas.Instance.Traducir("TextHerramienta");
        string tagNameJornal = DiccionarioIdiomas.Instance.Traducir("NombreJornal");
        string tagNameFertilizer1 = DiccionarioIdiomas.Instance.Traducir("TextAbono1P2");
        string tagNameFertilizer2 = DiccionarioIdiomas.Instance.Traducir("TextAbono2P2");
        string tagNameFertilizer3 = DiccionarioIdiomas.Instance.Traducir("TextAbono3P2");
        string tagNameMaterialOrganic = DiccionarioIdiomas.Instance.Traducir("TextMateriaOrganicaP2");
        string tagNameInsecticide1 = DiccionarioIdiomas.Instance.Traducir("TextInsecticida1P2");
        string tagNameInsecticide2 = DiccionarioIdiomas.Instance.Traducir("TextInsecticida2P2");
        string tagNameFungicide = DiccionarioIdiomas.Instance.Traducir("TextFungicidaP2");
        string tagNameHerbicide1 = DiccionarioIdiomas.Instance.Traducir("TextHerbicida1P2");
        string tagNameHerbicide2 = DiccionarioIdiomas.Instance.Traducir("TextHerbicida2P2");
        string tagNameCutBig = DiccionarioIdiomas.Instance.Traducir("TextTijerasGrandesP2");
        string tagNameCutLittle = DiccionarioIdiomas.Instance.Traducir("TextTijerasPequeñasP2");
        string tagNameStakes = DiccionarioIdiomas.Instance.Traducir("TextEstacasP2");
        string tagNameRollCabuya = DiccionarioIdiomas.Instance.Traducir("TextRollosCabuyaP2");
        string tagNameWater = DiccionarioIdiomas.Instance.Traducir("NombreAgua");
        string tagNameMeasure = DiccionarioIdiomas.Instance.Traducir("NombreAguaMedida");
        string tagNameFertilization = DiccionarioIdiomas.Instance.Traducir("TextFertilizacionP2");
        string tagNamePlaguesSicks = DiccionarioIdiomas.Instance.Traducir("NombrePlagasEnfermedades");
        string tagNameControllMalezas = DiccionarioIdiomas.Instance.Traducir("NombreControlMalezas");
        string tagNameTutorized = DiccionarioIdiomas.Instance.Traducir("TextTurorizadoP2");
        string tagNamePodaF = DiccionarioIdiomas.Instance.Traducir("TextPodaFormacionP2");
        string tagNamePodaS = DiccionarioIdiomas.Instance.Traducir("TextPodaSanitariaP2");
        string tagNameServiceGerminator = DiccionarioIdiomas.Instance.Traducir("NombreJornalGerminadero");
        string tagNamePlant = DiccionarioIdiomas.Instance.Traducir("NombrePlantas");
        string tagNamePlantUnidad = DiccionarioIdiomas.Instance.Traducir("NombrePlantasUnidad");
        
        switch (step)
        {
            case 0:
                if(fieldsFertilizer[0].text != "" && fieldsFertilizer[1].text != "" && fieldsFertilizer[2].text != "" && fieldsFertilizer[3].text != "")
                {
                    PanelFertilizacion.Instance.ShowPanel(false);
                    float countAB1 = float.Parse(fieldsFertilizer[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countAB2 = float.Parse(fieldsFertilizer[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countAB3 = float.Parse(fieldsFertilizer[2].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countAB4 = float.Parse(fieldsFertilizer[3].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Fertilizacion1", tagNameFertilizer1, tagNamePackage, countAB1, myValuesProductsP4.precioAbono1);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Fertilizacion2", tagNameFertilizer2, tagNamePackage, countAB2, myValuesProductsP4.precioAbono2);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Fertilizacion3", tagNameFertilizer3, tagNamePackage, countAB3, myValuesProductsP4.precioAbono3);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Fertilizacion4", tagNameMaterialOrganic, tagNamePackage, countAB4, myValuesProductsP4.precioMateriaOrganica);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                
                break;
            case 1:
                if(fieldsPlagasEnfermedades[0].text != "" && fieldsPlagasEnfermedades[1].text != "" && fieldsPlagasEnfermedades[2].text != "")
                {
                    PanelPlagasEnfermedades.Instance.ShowPanel(false);
                    float countFe1 = float.Parse(fieldsPlagasEnfermedades[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countFe2 = float.Parse(fieldsPlagasEnfermedades[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countFe3 = float.Parse(fieldsPlagasEnfermedades[2].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Enfermedades1", tagNameInsecticide1, tagNameLiters, countFe1, myValuesProductsP4.precioInsecticida1);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Enfermedades2", tagNameInsecticide2, tagNameLiters, countFe2, myValuesProductsP4.precioInsecticida2);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Enfermedades3", tagNameFungicide, tagNameLiters, countFe3, myValuesProductsP4.precioFungicida1);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 2:
                if(fieldsMalezas[0].text != "" && fieldsMalezas[1].text != "")
                {
                    PanelControlMalezas.Instance.ShowPanel(false);
                    float countHerbi1 = float.Parse(fieldsMalezas[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countHerbi2 = float.Parse(fieldsMalezas[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Malezas1", tagNameHerbicide1, tagNameLiters, countHerbi1, myValuesProductsP4.precioHerbicida1);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Malezas2", tagNameHerbicide2, tagNameLiters, countHerbi2, myValuesProductsP4.precioHerbicida2);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 3:
                if(fieldsPodas[0].text != "" && fieldsPodas[1].text != "")
                {
                    PanelPodas.Instance.ShowPanel(false);
                    float countTijeraGR = float.Parse(fieldsPodas[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countTijeraPq = float.Parse(fieldsPodas[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Poda1", tagNameCutBig, tagNameTool, countTijeraGR, myValuesProductsP4.precioTijerasGr);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Poda2", tagNameCutLittle, tagNameTool, countTijeraPq, myValuesProductsP4.precioTijerasPq);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 4:
                if(fieldsTutorado[0].text != "" && fieldsTutorado[1].text != "")
                {
                    PanelTutorado.Instance.ShowPanel(false);
                    float countEstaca = float.Parse(fieldsTutorado[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float cabuya = float.Parse(fieldsTutorado[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Tutorado1", tagNameStakes, tagNameTool, countEstaca, myValuesProductsP4.precioEstaca);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Tutorado2", tagNameRollCabuya, tagNameTool, cabuya, myValuesProductsP4.precioRolloCabuya);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                
                break;
            case 5:
                if(countAgua.text != "")
                {
                    float countAguaAux = float.Parse(countAgua.text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Agua", tagNameWater, tagNameMeasure, countAguaAux, myValuesProductsP4.precioAgua);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 6:
                if(CheckFields(fieldsResorceHuman1))
                {
                    PanelRecursoHumano1Situacion2.Instance.ShowPanel(false);
                    float countJor1 = float.Parse(fieldsResorceHuman1[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countJor2 = float.Parse(fieldsResorceHuman1[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countJor3 = float.Parse(fieldsResorceHuman1[2].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countJor4 = float.Parse(fieldsResorceHuman1[3].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countJor5 = float.Parse(fieldsResorceHuman1[4].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countJor6 = float.Parse(fieldsResorceHuman1[5].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("CostFijo", "Fertilizantes", tagNameFertilization, tagNameJornal, countJor1, myValuesProductsP4.precioJornal);
                    myInstantiateControll.InstanceProductInTablet("CostFijo", "Incecticidas", tagNamePlaguesSicks, tagNameJornal, countJor2, myValuesProductsP4.precioJornal);
                    myInstantiateControll.InstanceProductInTablet("CostFijo", "Herbicidas", tagNameControllMalezas, tagNameJornal, countJor3, myValuesProductsP4.precioJornal);
                    myInstantiateControll.InstanceProductInTablet("CostFijo", "Tutorizado", tagNameTutorized, tagNameJornal, countJor4, myValuesProductsP4.precioJornal);
                    myInstantiateControll.InstanceProductInTablet("CostFijo", "PodaF", tagNamePodaF, tagNameJornal, countJor5, myValuesProductsP4.precioJornal);
                    myInstantiateControll.InstanceProductInTablet("CostFijo", "PodaS", tagNamePodaS, tagNameJornal, countJor6, myValuesProductsP4.precioJornal);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 7:
                if(fieldResourceHuman2.text != "")
                {
                    PanelRecursoHumano2Situacion2.Instance.ShowPanel(false);
                    float countJorGer = float.Parse(fieldResourceHuman2.text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("CostVariable", "Germinadero", tagNameServiceGerminator, tagNameJornal, countJorGer, myValuesProductsP4.precioJornal);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 8:
                if(countPlants.text != "")
                {
                    float countPl1 = float.Parse(countPlants.text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("CostFijo", "cantPlantas", tagNamePlant, tagNamePlantUnidad, countPl1, myValuesProductsP4.precioPlanta);    
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
        }
    }

    public void ButtonAsignPanel(GameObject myGameObject)
    {
        panelNext = myGameObject;
    }

    public void EnableButtonAceptar(int posPanel)
    {
        switch(posPanel)
        {
            case 0:
                if(fieldDimensionCultivo[0].text != "" && fieldDimensionCultivo[1].text != "")
                {
                    float auxDi = float.Parse(fieldDimensionCultivo[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float auxDi2 = float.Parse(fieldDimensionCultivo[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    if(auxDi >= 30 && auxDi <= 40 && auxDi2 >= 3 && auxDi2 <= 12)
                    {
                        panelsRegisterParametres[0].SetActive(false);
                        panelNext.SetActive(true);
                    }
                    else
                    {
                        BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("TextRangoDimensionesP4"),DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), null, false);
                    }
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 1:
                if(fieldsDistribucionCultivo[0].text != "" && fieldsDistribucionCultivo[1].text != "" && fieldsDistribucionCultivo[2].text != "")
                {
                    float auxD1 = float.Parse(fieldsDistribucionCultivo[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float auxD2 = float.Parse(fieldsDistribucionCultivo[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float auxD3 = float.Parse(fieldsDistribucionCultivo[2].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    if(auxD1 >= 30 && auxD1 <= 120 && auxD2 >= 0 && auxD2 <= 40 && auxD3 >= 0 && auxD3 <= 40)
                    {
                        panelsRegisterParametres[1].SetActive(false);
                        panelNext.SetActive(true);
                    }
                    else
                    {
                        BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("TextRangoDistribucionP4"),DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), null, false);
                    }
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 2:
                if(fieldsFertilizacionYRiego[0].text != "" && fieldsFertilizacionYRiego[1].text != "")
                {
                    float auxFer = float.Parse(fieldsFertilizacionYRiego[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float auxFer2 = float.Parse(fieldsFertilizacionYRiego[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    if(auxFer >= 5 && auxFer <= 10 && auxFer2 >= 6 && auxFer2 <= 10)
                    {
                        panelsRegisterParametres[2].SetActive(false);
                        panelNext.SetActive(true);
                    }
                    else
                    {
                        BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("TextRangoFertilizacionP4"),DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), null, false);
                    }
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 3:
                if(fieldsTutorizadoYPoda[0].text != "" && fieldsTutorizadoYPoda[1].text != "" && fieldsTutorizadoYPoda[2].text != "")
                {
                    float auxTu = float.Parse(fieldsTutorizadoYPoda[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float auxTu2 = float.Parse(fieldsTutorizadoYPoda[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float auxTu3 = float.Parse(fieldsTutorizadoYPoda[2].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    if(auxTu >= 10 && auxTu <= 15 && auxTu2 >= 1 && auxTu2 <= 2 && auxTu3 >= 1 && auxTu3 <= 2)
                    {
                        panelsRegisterParametres[3].SetActive(false);
                        panelNext.SetActive(true);
                    }
                    else
                    {
                        BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("TextRangoTutorizadoYPodasP4"),DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), null, false);
                    }
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 4:
                if(fieldsPlagasEnfermedadesMaleza[0].text != "" && fieldsPlagasEnfermedadesMaleza[1].text != "")
                {
                    float auxPla = float.Parse(fieldsPlagasEnfermedadesMaleza[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float auxPla2 = float.Parse(fieldsPlagasEnfermedadesMaleza[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    if(auxPla >= 5 && auxPla <= 6 && auxPla2 >= 1 && auxPla2 <= 2)
                    {
                        ButtonAcpetarFinish();
                        PanelPlagasEnfermedadesMalezas.Instance.ShowPanel(false);
                    }
                    else
                    {
                        BoxMessageManager.Instance.CreateBoxMessageInfo(DiccionarioIdiomas.Instance.Traducir("TextRangoEnfermedadesYMalezasP4"),DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), null, false);
                    }
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
        }
    }

    public void LastPanelFuntion()
    {
        if(fieldsPlagasEnfermedadesMaleza[0].text != "" && fieldsPlagasEnfermedadesMaleza[1].text != "")
        {
            buttonNoInteractable.interactable = true;
        }
        else
        {
            buttonNoInteractable.interactable = false;
        }
    }

    public void ButtonAcpetarFinish()
    {
        myValuesProductsP4.terrainLargo =  int.Parse(fieldDimensionCultivo[0].text);
        myValuesProductsP4.terrainAncho = int.Parse(fieldDimensionCultivo[1].text);
        myValuesProductsP4.tiempoCultivo = int.Parse(fieldsDistribucionCultivo[0].text);
        myValuesProductsP4.distanciaCalleMayor = float.Parse(fieldsDistribucionCultivo[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        myValuesProductsP4.distanciaPlantas = float.Parse(fieldsDistribucionCultivo[2].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        myValuesProductsP4.repFertilizacion = int.Parse(fieldsFertilizacionYRiego[0].text);
        myValuesProductsP4.repRiego = int.Parse(fieldsFertilizacionYRiego[1].text);
        myValuesProductsP4.repTutorado = int.Parse(fieldsTutorizadoYPoda[0].text);
        myValuesProductsP4.repPodasFormacion = int.Parse(fieldsTutorizadoYPoda[1].text);
        myValuesProductsP4.repPodasSanitarias = int.Parse(fieldsTutorizadoYPoda[2].text);
        myValuesProductsP4.repPlagasEnfermedades = int.Parse(fieldsPlagasEnfermedadesMaleza[0].text);
        myValuesProductsP4.repMalezas = int.Parse(fieldsPlagasEnfermedadesMaleza[1].text);
        GameObject ventanaOscura = GameObject.Find("ventanaOscura");
        ventanaOscura.SetActive(false);        
    }

    public void ButtonRestar()
    {
        RestarPractice();
    }

    public void ScreenShootScenary()
    {
        StartCoroutine(ScreenShootScenaryCorutin());
    }

    IEnumerator ScreenShootScenaryCorutin()
    {
        cameraCaptureScenary.SetActive(true);
        yield return new WaitForEndOfFrame();
        cameraCaptureScenary.SetActive(false);
    }

    bool CheckFields(TMP_InputField[] fieldTest)
    {
        int test = 0;

        for(int i = 0; i < fieldTest.Length; i++)
        {
            if(fieldTest[i].text == "")
            {
                test = 1;
            }
        }
        return test > 0 ? false : true;
    }

    public void SaveConfirmation(TextMeshProUGUI textConfirmation)
    {
        StartCoroutine(EventSaveConfirmation(textConfirmation));
    }

    IEnumerator EventSaveConfirmation(TextMeshProUGUI textConfirmation)
    {
        textConfirmation.transform.gameObject.SetActive(true);
        yield return new WaitForSeconds(waitTimeConfirmation);
        textConfirmation.transform.gameObject.SetActive(false);
    }

    void RestarPractice()
    {
        for(int i = 0; i < containerTablet.transform.childCount; i++)
        {
            GameObject auxGO = containerTablet.transform.GetChild(i).gameObject;
            myRegistroProductoController = auxGO.GetComponent<RegistroProductoController>();
            
            float priceToRemove = 0;
            float lastPrice = float.Parse(auxGO.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text);
            float lastCount = float.Parse(auxGO.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text);
            priceToRemove = lastPrice * lastCount;
            myInstantiateControll.RemovePriceToCorrectZone(auxGO.tag, priceToRemove);
            myInstantiateControll.sumTotal.Remove(priceToRemove);
            myInstantiateControll.RealiceSumOfPrice();
            myRegistroProductoController.DeleteObjectInScene();
            Destroy(auxGO);
        }

        for(int j = 0; j < containerPanels.transform.childCount; j++)
        {
            containerPanels.transform.GetChild(j).gameObject.SetActive(false);
        }

        PanelIngreseVolumenAgua.Instance.ShowPanel(false);
        PanelIngreseCantidadPlantulas.Instance.ShowPanel(false);

        for(int k = 0; k < containerIncreases.Length; k++)
        {
            containerIncreases[k].SetActive(false);
        }

        PanelDimensionesCultivo.Instance.ShowPanel(true);
        myControllArrowBack.UpdateActualStateAndActiveZones(1);
    }
}