﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NSScriptableValues;
using NSInterfazAvanzada;

public class CalificacionManagerP4 : MonoBehaviour
{
    [Header("Referencias a Scripts")]
    [SerializeField] private RegisterDatesP4 myRegisterDates4;
    [SerializeField] private CalculoCantidadProductosP4 myCalculoCantidadProductos;

    [Header("Referencia a Scriptable Objetc")]
    [SerializeField] private ValueFloat valCalificationRegisterData4;
    [SerializeField] private ValueFloat valCalificationSingleTables4;

    public void CheckCalificationFinal()
    {
        valCalificationSingleTables4.Value = myCalculoCantidadProductos.calificacion1;
        valCalificationRegisterData4.Value = myRegisterDates4.calificacion2;
    }
}
