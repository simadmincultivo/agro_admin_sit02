﻿﻿using UnityEngine;

namespace NSCalificacionSituacion
{
    [CreateAssetMenu(fileName = "SOSessionData", menuName = "NSCalificacionSituacion/SOSessionData", order = 0)]
    public class SOSessionData : ScriptableObject
    {
        public byte quantityAttempts;

        public float timeSituationFloat;

        public string TimeSituationString
        {
            get
            {
                var tmpSegundos = Mathf.Floor(timeSituationFloat % 60);
                var tmpMinutos = Mathf.Floor(timeSituationFloat / 60f);
                var tmpHoras = Mathf.Floor(tmpMinutos/60);
                return (tmpHoras < 10f? "0" + tmpHoras : tmpHoras.ToString()) + ":" + (tmpMinutos % 60 < 10f ? "0" + tmpMinutos % 60 : (tmpMinutos % 60).ToString()) + ":" + (tmpSegundos < 10f ? "0" + tmpSegundos : tmpSegundos.ToString());
            }
        }
    }
}