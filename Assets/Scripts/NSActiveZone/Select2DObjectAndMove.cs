﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;

namespace NSActiveZone
{
    public class Select2DObjectAndMove : MonoBehaviour
    {
        [Tooltip("Layer para restringir la seleccion de objetos")] [SerializeField]
        private LayerMask layerSelectionObjects2D;

        [Tooltip("Camara desde donde se seleccionan los objetos")] [SerializeField]
        private Camera refCamera;

        [Tooltip("Radio alrededor del touch que se tiene encuenta para seleccionar los objetos")] [SerializeField]
        private float radiusSelection = 0.2f;

        /// <summary>
        /// Objeto actualmente seleccioando
        /// </summary>
        private AbstractObject2D refAbstractObject2DSelected;

        private float touchScreenDelta;
        
        private void Awake()
        {
            if (!refCamera)
                refCamera = Camera.main;
        }


        private void OnEnable()
        {
            LeanTouch.OnFingerDown += OnFingerDown;
            LeanTouch.OnFingerSet += OnFingerSet;
            LeanTouch.OnFingerTap += OnFingerTab;
            LeanTouch.OnFingerUp += OnFingerUp;
        }

        private void OnDisable()
        {
            LeanTouch.OnFingerDown -= OnFingerDown;
            LeanTouch.OnFingerSet -= OnFingerSet;
            LeanTouch.OnFingerTap -= OnFingerTab;
            LeanTouch.OnFingerUp -= OnFingerUp;
        }

        private void OnFingerDown(LeanFinger argLeanFinger)
        {
            //Debug.Log("OnFingerDown");
            var tmpGuiElements = LeanTouch.RaycastGui(argLeanFinger.ScreenPosition);

            if (tmpGuiElements.Count > 0) //si hay interfaz por encima
                return;

            var tmpPositionOnWorld = refCamera.ScreenToWorldPoint(argLeanFinger.ScreenPosition);
            var tmpCollision2D = Physics2D.OverlapCircle(tmpPositionOnWorld, radiusSelection, layerSelectionObjects2D);

            if (tmpCollision2D)
            {
                refAbstractObject2DSelected = tmpCollision2D.GetComponent<AbstractObject2D>();

                if (refAbstractObject2DSelected && refAbstractObject2DSelected.ActualAction == ActualAction.WaitingForAction)
                {
                    refAbstractObject2DSelected.PositionObjective = tmpPositionOnWorld;
                    refAbstractObject2DSelected.OnSelected();
                }
            }
        }

        private void OnFingerSet(LeanFinger argLeanFinger)
        {
            if (argLeanFinger.ScreenDelta.magnitude > touchScreenDelta)
                touchScreenDelta = argLeanFinger.ScreenDelta.magnitude;
             
            if (touchScreenDelta > 0.05f)
                if (refAbstractObject2DSelected)
                {
                    refAbstractObject2DSelected.PositionObjective = refCamera.ScreenToWorldPoint(argLeanFinger.ScreenPosition);
                    refAbstractObject2DSelected.OnMovement();
                    //Debug.Log("OnFingerSet");
                }
        }

        private void OnFingerTab(LeanFinger argLeanFinger)
        {
            //Debug.Log("OnFingerTab");

            var tmpGuiElements = LeanTouch.RaycastGui(argLeanFinger.ScreenPosition);

            if (tmpGuiElements.Count > 0) //si hay interfaz por encima
                return;

            var tmpPositionOnWorld = refCamera.ScreenToWorldPoint(argLeanFinger.ScreenPosition);
            var tmpCollision2D = Physics2D.OverlapCircle(tmpPositionOnWorld, radiusSelection, layerSelectionObjects2D);

            if (tmpCollision2D)
            {
                refAbstractObject2DSelected = tmpCollision2D.GetComponent<AbstractObject2D>();

                if (refAbstractObject2DSelected)
                {
                    if (refAbstractObject2DSelected.ActualAction == ActualAction.WaitingForAction)
                        refAbstractObject2DSelected.OnTab();

                    return;
                }

                var tmpActiveZone = tmpCollision2D.GetComponent<ActiveZoneOnWorld>();

                if (tmpActiveZone)
                    tmpActiveZone.OnTab();
            }
        }

        private void OnFingerUp(LeanFinger argLeanFinger)
        {
            //Debug.Log("OnFingerUp");
            touchScreenDelta = 0f;

            if (refAbstractObject2DSelected)
                refAbstractObject2DSelected.OnSelectFinish();

            refAbstractObject2DSelected = null;
        }
    }
}