﻿using UnityEngine;

namespace NSActiveZone.Ejemplo
{
    public class ObjetoA : AbstractObject2D
    {
        /*
         * 0 = Posicion inicial
         * 1 = se translado a la zona activa 2
         * 2 = se preciono tab/click sobre el y habilito la zona activa 3
         * 3 = Se preciono click sobre la zona activa 3
         */
        
        protected override void Awake()
        {
            base.Awake();
            //ActivateActiveZone("ZonaActiva0ObjetoA", false);
            //ActivateActiveZone("ZonaActiva0ObjetoA");
            //UpdateStageActiveZone("ZonaActiva0ObjetoA", 2);
            //ActivateAllActiveZonesOfActualStage(false);
        }

        protected override void Update()
        {
            base.Update();
        }

        #region implementations

        protected override void OnReleaseObjectOverActiveZone(string argNameActiveZone)
        {
            if (argNameActiveZone.Equals("NombreZonaActiva"))
            {
                switch (actualStageIndex)
                {
                    case 0:
                        
                        break;
                    
                    case 1:
                        
                        break;
                    
                    //etc
                }
            }
        }

        protected override void OnReleaseObjectOverNothing()
        {
            Debug.Log("Se solto el objeto en el aire, lejos de cualquier zona activa");
            
            switch (actualStageIndex)
            {
                case 0:
                        
                    break;
                    
                case 1:
                        
                    break;
                    
                //etc
            }
        }

        public override void OnTab()
        {
            switch (actualStageIndex)
            {
                case 0:
                    
                    break;
                
                case 1:
                    
                    break;
                
                //etc
            }
        }

        public override void OnTabOverActiveZone(string argNameActiveZone)
        {
            if (argNameActiveZone.Equals("ZonaActiva0ObjetoA"))
            {
                switch (actualStageIndex)
                {
                    case 0:
                        actualStageIndex = 1;
                        //ActivateActiveZone("ZonaActiva0ObjetoA", false);
                        //ActivateActiveZone("ZonaActiva1ObjetoA");
                        break;
                    
                    case 1:
                        
                        break;
                    
                    //etc
                }
            }
        }

        public override void OnMoving()
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}