using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace NSActiveZone
{
    [RequireComponent(typeof(PolygonCollider2D), typeof(SpriteRenderer))]
    public class ActiveZoneOnWorld : MonoBehaviour
    {
        #region members

        [Header("Configuration Zona Activa")]

        [Tooltip("Activar la zona activa cuando se selecciona el objeto a la que esta se relaciona?")]
        [SerializeField] private bool activeZoneWhenSelectObject;

        [Tooltip("Indice relacionado con el indice de la etapa actual del objeto, para activar la zona activa si coincide este indice con el de la etapa del objeto")]
        [SerializeField] private int activeZoneRelatedIndexWithStage;

        [Tooltip("La zona activa es un punto, o un area sobre el mundo?")]
        [SerializeField] private bool isActiveZoneArea;

        [Tooltip("Punto de la zona activa en el que se hubicara el objeto dueño de esta zona activa")]
        [SerializeField] private Transform tranformPointTarget;
        
        [Tooltip("Distancia minima para moverse al punto de la zona activa")]
        [SerializeField] private float minDistanceToMovement = 0.75f;
        
        private int isActive;
        
        public UnityAction<string> delegateOnTab;
        
        [Header("Visualizacion Zona Activa")]
        [SerializeField] private float animationUmbral = 1f;

        [SerializeField] private float animationVelocity = 1f;
        
        private SpriteRenderer spriteRendererImagen;
        
        private PolygonCollider2D refPolygonCollider2D;
        #endregion

        #region propierties
        
        
        public bool IsActive
        {
            get => isActive > 0;
        }

        public PolygonCollider2D RefPolygonCollider2D => refPolygonCollider2D;

        public bool ActiveZoneWhenSelectObject => activeZoneWhenSelectObject;

        public int ActiveZoneRelatedIndexWithStage
        {
            set => activeZoneRelatedIndexWithStage = value;
            get => activeZoneRelatedIndexWithStage;
        }

        public bool IsActiveZoneArea => isActiveZoneArea;

        public Transform TranformPointTarget => tranformPointTarget;

        public float MinDistanceToMovement => minDistanceToMovement;
        #endregion

        #region MonoBehaviour

        private void Awake()
        {
            spriteRendererImagen = GetComponent<SpriteRenderer>();
            refPolygonCollider2D = GetComponent<PolygonCollider2D>();
            gameObject.SetActive(IsActive);
        }

        private void OnEnable()
        {
            StartCoroutine(CouZoneActiveAnimation());
        }

        private void OnDisable()
        {
            StopCoroutine(CouZoneActiveAnimation());
        }
        #endregion
        

        #region public methods

        public void OnTab()
        {
            if (IsActive)
                delegateOnTab(name);
        }
        
         /// <summary>
        /// Calcula la distancia que hay hasta la zona activa
        /// </summary>
        /// <param name="argPositionTouch">Posicion desde la cual se evalua la distancia</param>
        /// <param name="argPolygonCollider2D">Collider2D del objeto para chekear si esta sobre el area de la zona activa y retornar distancia 0 en caso de que se encuentre encima</param>
        /// <returns>Distancia que hay hasta el punto de la zona activa.</returns>
        public float GetDistanceToActiveZone(Vector2 argPositionTouch, PolygonCollider2D argPolygonCollider2D)
        {
            if (isActiveZoneArea)
            {
                var tmpColliderFilter = new ContactFilter2D();
                tmpColliderFilter.SetLayerMask(LayerMask.GetMask("Object2DSelectable"));
                tmpColliderFilter.useTriggers = false;

                var tmpColliderOverlap = new Collider2D[1];
                refPolygonCollider2D.OverlapCollider(tmpColliderFilter, tmpColliderOverlap);

                Debug.Log("tmpColliderOverlap " + tmpColliderOverlap[0]);
                
                foreach (var tmpCollider2D in tmpColliderOverlap)
                    if (tmpCollider2D  == argPolygonCollider2D)
                        return 0f;

                return Mathf.Infinity;
            }

            var tmpDistanceToActiveZone = Vector2.Distance(argPositionTouch, tranformPointTarget.position);
            
            if (tmpDistanceToActiveZone <= minDistanceToMovement)
                return tmpDistanceToActiveZone;

            return Mathf.Infinity;
        }

        /// <summary>
        /// Calcula la posicion de la zona activa
        /// </summary>
        /// <param name="argActualPosition">Posicion actual desde donde se calcula</param>
        /// <returns>Posicion de la zona activa calculada</returns>
        public Vector2 GetPointOverActiveZone(Vector2 argActualPosition)
        {
            if (isActiveZoneArea)
                return argActualPosition;

            return tranformPointTarget.position;
        }

        /// <summary>
        /// Activa la animacion de la zona activa
        /// </summary>
        /// <param name="argActivate">Activar?</param>
        public void ActiveActiveZone(bool argActivate = true)
        {
            isActive += argActivate ? 1 : -1;
            gameObject.SetActive(IsActive);
        }
        
        /// <summary>
        /// Activa la animacion de la zona activa, sirve para la zona activa propia
        /// </summary>
        /// <param name="argActivate">Activar?</param>
        public void ActiveActiveZoneFixed(bool argActivate = true)
        {
            isActive = argActivate ? 1 : -1;
            gameObject.SetActive(IsActive);
        }
        #endregion

        #region Courutines

        private IEnumerator CouZoneActiveAnimation()
        {
            var tmpAngle = 0f;

            while (true)
            {
                if (IsActive)
                {
                    tmpAngle += (180f / animationVelocity) * Time.deltaTime;
                    tmpAngle %= 180f;
                    spriteRendererImagen.color = new Color(1, 1, 1, Mathf.Sin(tmpAngle * Mathf.Deg2Rad) * animationUmbral);
                }
                else
                    spriteRendererImagen.color = new Color(1, 1, 1, 0);

                yield return null;
            }
        }
        #endregion
    }
}