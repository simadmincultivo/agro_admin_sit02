﻿#pragma warning disable 0649
using NSUtilities;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace NSActiveZones
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class ActiveZoneButton : MonoBehaviour, IButtonWorld
    {
        #region members

        [SerializeField] private SpriteRenderer spriteRendererImagen;

        [SerializeField] private float animationVelocity = 1;

        [SerializeField] private float animationUmbral = 1;

        [Header("Event"), Space(10)] public UnityEvent OnButtonClickDown;

        [SerializeField] private bool interactuable = true;

        private BoxCollider2D refBoxCollider2D;

        #endregion

        #region accesor

        public bool _interactuable
        {
            set
            {
                interactuable = value;
                spriteRendererImagen.enabled = interactuable;

                if (!refBoxCollider2D)
                    refBoxCollider2D = GetComponent<BoxCollider2D>();

                refBoxCollider2D.enabled = interactuable;
            }
        }

        #endregion

        private void OnEnable()
        {
            StartCoroutine(CouButtonAnimation());
        }

        public void OnClickDown()
        {
            if (!interactuable)
                return;

            Debug.Log("OnClickDown : " + gameObject.name);
            OnButtonClickDown.Invoke();
        }

        private IEnumerator CouButtonAnimation()
        {
            var tmpAngle = 0f;

            while (true)
            {
                tmpAngle += animationVelocity * Time.deltaTime;
                tmpAngle %= 180f;
                spriteRendererImagen.color = new Color(1, 1, 1, Mathf.Sin(tmpAngle * Mathf.Deg2Rad) * animationUmbral);
                yield return null;
            }
        }
    }
}