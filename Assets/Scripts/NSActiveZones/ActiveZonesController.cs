﻿#pragma warning disable 0649
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NSActiveZones
{
    public class ActiveZonesController : MonoBehaviour
    {
        #region members

        [SerializeField]
        private List<ActiveZone> listActiveZones;

        private Dictionary<string, ActiveZone> dictonaryActivesZones = new Dictionary<string, ActiveZone>();
        
        private bool initialized;

        private int counterDesactiveAllZones = -1;
        #endregion

        #region propierties

        public int CounterDesactiveAllZones
        {
            get { return counterDesactiveAllZones; }
        }

        #endregion
        
        #region Event

        [Header("Events")]
        public UnityEvent OnActiveAllZones;
        #endregion

        #region public method

        public void SetZoneInteractuable(string argNameActiveZone, bool argInteractuable)
        {
            dictonaryActivesZones[argNameActiveZone].InteractuableZones(argInteractuable);
        }

        public void SetZoneInteractuableFalse(string argNameActiveZone)
        {
            dictonaryActivesZones[argNameActiveZone].InteractuableZones(false);
        }

        public void SetZoneInteractuableTrue(string argNameActiveZone)
        {
            dictonaryActivesZones[argNameActiveZone].InteractuableZones(true);
        }

        public void ActiveZone(string argNameActiveZone, bool argActivate)
        {
            InitActivesZones();
            dictonaryActivesZones[argNameActiveZone].ActiveZones(argActivate);
        }

        public void ActiveZone(string argNameActiveZone)
        {
            InitActivesZones();
            dictonaryActivesZones[argNameActiveZone].ActiveZones(true);
        }

        public void InteractuableZone(string argNameActiveZone, bool argInteractuable)
        {
            InitActivesZones();
            dictonaryActivesZones[argNameActiveZone].InteractuableZones(argInteractuable);
        }

        public void DesactivateZone(string argNameActiveZone)
        {
            InitActivesZones();
            dictonaryActivesZones[argNameActiveZone].ActiveZones(false);
        }

        public void ActiveSpecificZone(string argNameActiveZone, int[] argIndexZone)
        {
            InitActivesZones();
            dictonaryActivesZones[argNameActiveZone].ActiveSpecificZone(argIndexZone);
        }

        public void DesactiveAllZones()
        {
            InitActivesZones();

            counterDesactiveAllZones--;
            Debug.Log("counterDesactiveAllZones--");

            foreach (var tmpActiveZone in dictonaryActivesZones)
                tmpActiveZone.Value.ActiveZones(false);
        }

        public void ActiveAllZones()
        {
            InitActivesZones();
            counterDesactiveAllZones++;
            Debug.Log("counterDesactiveAllZones++");

            if (counterDesactiveAllZones == 0)
            {
                foreach (var tmpActiveZone in dictonaryActivesZones)
                    tmpActiveZone.Value.ActiveZones(true);

                OnActiveAllZones.Invoke();
            }
        }
        #endregion

        #region private method

        private void InitActivesZones()
        {
            if (!initialized)
            {
                for (int i = 0; i < listActiveZones.Count; i++)
                    dictonaryActivesZones.Add(listActiveZones[i]._nameActiveZone, listActiveZones[i]);

                initialized = true;
            }
        }
        #endregion        
    }

    [Serializable]
    public class ActiveZone
    {
        #region members

        [SerializeField]
        private string nameActiveZone;

        [SerializeField]
        private GameObject[] activesZonesGameobjects;
        #endregion

        #region accesores

        public string _nameActiveZone
        {
            get
            {
                return nameActiveZone;
            }
        }
        #endregion

        #region public methods
        public void ActiveZones(bool argActivate)
        {
            foreach (var tmpZone in activesZonesGameobjects)
                tmpZone.SetActive(argActivate);
        }

        public void InteractuableZones(bool argInteractuable)
        {
            foreach (var tmpZone in activesZonesGameobjects)
                tmpZone.GetComponent<ActiveZoneButton>()._interactuable = argInteractuable;
        }

        public void ActiveSpecificZone(int[] argIndexZone)
        {
            for (int i = 0; i < activesZonesGameobjects.Length; i++)
            {
                activesZonesGameobjects[i].GetComponent<ActiveZoneButton>()._interactuable = false;
                activesZonesGameobjects[i].SetActive(false);
            }

            for (int j = 0; j < argIndexZone.Length; j++)
            {
                activesZonesGameobjects[argIndexZone[j]].GetComponent<ActiveZoneButton>()._interactuable = true;
                activesZonesGameobjects[argIndexZone[j]].SetActive(true);
            }

            Debug.Log("ActiveSpecificZone : " + argIndexZone);
        }
        #endregion
    }
}