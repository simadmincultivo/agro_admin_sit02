#pragma warning disable 0649
using UnityEngine;

namespace NSActiveZones
{
    public class ComponentDependZoneActive : MonoBehaviour
    {
        #region members

        [SerializeField] private ActiveZonesController refActiveZonesController;

        [SerializeField] private MonoBehaviour refMonoBehaviourComponent;

        #endregion

        #region mono behaviour

        private void Update()
        {
            refMonoBehaviourComponent.enabled = refActiveZonesController.CounterDesactiveAllZones == 0;
        }
        #endregion
    }
}