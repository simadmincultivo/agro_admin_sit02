﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSActiveZone;
using NSInterfaz;

public class StructureControllP3 : AbstractObject2D
{
    [Header("Referencias a Scripts")]
    [SerializeField] private ControllArrowBack myControllArrowBack;

    [Header("Fondos de cada edificio")]
    [SerializeField] private GameObject[] backgroundsP3;

    void Start()
    {
        actualStageIndex = 1;
        ActivateAllActiveZonesOfActualStage(true);
    }

    public override void OnMoving()
    {
        
    }

    public override void OnTab()
    {
        
    }

    public override void OnTabOverActiveZone(string argNameActiveZone)
    {
        if(argNameActiveZone.Equals("ZoneGreenHouseP3"))
        {
            backgroundsP3[0].SetActive(true);
            myControllArrowBack.selectedGreenHouseP3 = true;
            DisableZones(1);
        }
        else if(argNameActiveZone.Equals("ZoneCollectionCenter"))
        {
            backgroundsP3[1].SetActive(true);
            myControllArrowBack.selectedCollectionCenter = true;
            PanelIngreseCantidadCanastillas.Instance.ShowPanel();
            DisableZones(1);
        }
        else if(argNameActiveZone.Equals("ZoneSelectionAndPackaging"))
        {
            backgroundsP3[2].SetActive(true);
            myControllArrowBack.selectedSelectionAndPackaging = true;
            PanelIngreseCantidadCanastillas2.Instance.ShowPanel();
            PanelIngreseCantidadBalanzas.Instance.ShowPanel();
            DisableZones(1);
            ActivateActiveZone("ZoneBalanza");
        }
        else if(argNameActiveZone.Equals("ZoneBalanza"))
        {
            PanelZoomBalanza.Instance.ShowPanel();
        }
    }

    protected override void OnReleaseObjectOverActiveZone(string argNameActiveZone)
    {
        
    }

    protected override void OnReleaseObjectOverNothing()
    {
        
    }

    public void ButtonHumanResources()
    {
        if(myControllArrowBack.selectedGreenHouseP3)
        {
            PanelRecursoHumano1Situacion3.Instance.ShowPanel();
        }
        else if(myControllArrowBack.selectedCollectionCenter)
        {
            PanelRecursoHumano2Situacion3.Instance.ShowPanel();
        }
        else if(myControllArrowBack.selectedSelectionAndPackaging)
        {
            PanelRecursoHumano3Situacion3.Instance.ShowPanel();
        }
        else
        {
            PanelRecursoHumano4Situacion3.Instance.ShowPanel();
        }
    }
    void DisableZones(int state)
    {
        actualStageIndex = state;
        ActivateAllActiveZonesOfActualStage(false);
    }
}
