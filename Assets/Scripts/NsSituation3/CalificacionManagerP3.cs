﻿using System.Collections;
using UnityEngine;
using TMPro;
using NSScriptableValues;
using NSInterfazAvanzada;

public class CalificacionManagerP3 : MonoBehaviour
{
    [Header("Referencias a Scripts")]
    [SerializeField] private RegisterOfDatesP3 myRegisterDates3;
    [SerializeField] private CalculoCantidadProductosP3 myCalculoCantidadProductos;

    [Header("Referencias a Scriptable Objects")]
    [SerializeField] private ValueFloat valCalificationRegisterData3;
    [SerializeField] private ValueFloat valCalificationSingleTables3;

    public void CheckCalificationFinal()
    {
        valCalificationSingleTables3.Value = myCalculoCantidadProductos.calificacion1;
        valCalificationRegisterData3.Value = myRegisterDates3.calificacion2;
    }
}
