﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using NSInterfaz;
using NSTraduccionIdiomas;
using NSSceneLoading;

public class ControllInteractionsP2 : MonoBehaviour
{
    [Header("Instancias de scripts")]
    [SerializeField] private ValuesProductosManagerP2 myValuesProductsP2;
    [SerializeField] private InstantiateController myInstantiateControll;
    [SerializeField] private ControllArrowBack myControllArrowBack;
    private RegistroProductoController myRegistroProductoController;

    [Header("Objetos de bodega")]
    public TMP_InputField[] fieldsFertilizer;
    public TMP_InputField[] fieldsPlagasEnfermedades;
    public TMP_InputField[] fieldsMalezas;
    public TMP_InputField[] fieldsPodas;
    public TMP_InputField[] fieldsTutorado;

    [Header("Objetos de hangar")]
    public TMP_InputField countAgua;

    [Header("Objetos de invernadero")]
    [SerializeField] private TMP_Dropdown[] dropsResorceHuman1;
    
    [Header("Objetos de germinadero")]
    [SerializeField] private TMP_Dropdown dropResourceHuman2;
    [SerializeField] private TMP_InputField countPlants;

    [Header("Camara captura de escenario")]
    [SerializeField] private GameObject cameraCaptureScenary;

    [Header("Tiempo y texto de espera para desaparecer texto de confirmación")]
    [SerializeField] private float waitTimeConfirmation;

    [Header("Contenedor de objetos de tableta")]
    [SerializeField] private GameObject containerTablet;

    [Header("Referencia de canvas en escena")]
    [SerializeField] private GameObject containerPanels;

    [Header("Contenedor de aumentos")]
    [SerializeField] private GameObject[] containerIncreases;

    private float numProduct;

    [SerializeField]
    private PanelBarraUsuario barraUsuario;
    [SerializeField]
    private PanelParametrosSimulacion panelDeInicioPracticas;


    private void Start()
    {

        if (PlayerPrefs.HasKey("intentos"))
        {
            barraUsuario.LoadAttempts(PlayerPrefs.GetInt("intentos"));
            StartCoroutine(activarmanelpractica());
            PlayerPrefs.DeleteKey("intentos");
        }
    }

    IEnumerator activarmanelpractica()
    {
        yield return new WaitForEndOfFrame();
        activarBienvenida();
       // panelDeInicioPracticas.ShowPanel(true);
    }

    public void activarBienvenida()
    {
        PanelInterfazBienvenida2.Instance.ShowPanel();
    }


    public void ButtonAceptarP2(int step)
    {
        string tagNamePackage = DiccionarioIdiomas.Instance.Traducir("NombreBultos");
        string tagNameLiters = DiccionarioIdiomas.Instance.Traducir("NombreLitros");
        string tagNameTool = DiccionarioIdiomas.Instance.Traducir("TextHerramienta");
        string tagNameJornal = DiccionarioIdiomas.Instance.Traducir("NombreJornal");
        string tagNameFertilizer1 = DiccionarioIdiomas.Instance.Traducir("TextAbono1P2");
        string tagNameFertilizer2 = DiccionarioIdiomas.Instance.Traducir("TextAbono2P2");
        string tagNameFertilizer3 = DiccionarioIdiomas.Instance.Traducir("TextAbono3P2");
        string tagNameMaterialOrganic = DiccionarioIdiomas.Instance.Traducir("TextMateriaOrganicaP2");
        string tagNameInsecticide1 = DiccionarioIdiomas.Instance.Traducir("TextInsecticida1P2");
        string tagNameInsecticide2 = DiccionarioIdiomas.Instance.Traducir("TextInsecticida2P2");
        string tagNameFungicide = DiccionarioIdiomas.Instance.Traducir("TextFungicidaP2");
        string tagNameHerbicide1 = DiccionarioIdiomas.Instance.Traducir("TextHerbicida1P2");
        string tagNameHerbicide2 = DiccionarioIdiomas.Instance.Traducir("TextHerbicida2P2");
        string tagNameCutBig = DiccionarioIdiomas.Instance.Traducir("TextTijerasGrandesP2");
        string tagNameCutLittle = DiccionarioIdiomas.Instance.Traducir("TextTijerasPequeñasP2");
        string tagNameStakes = DiccionarioIdiomas.Instance.Traducir("TextEstacasP2");
        string tagNameRollCabuya = DiccionarioIdiomas.Instance.Traducir("TextRollosCabuyaP2");
        string tagNameWater = DiccionarioIdiomas.Instance.Traducir("NombreAgua");
        string tagNameMeasure = DiccionarioIdiomas.Instance.Traducir("NombreAguaMedida");
        string tagNameFertilization = DiccionarioIdiomas.Instance.Traducir("TextFertilizacionP2");
        string tagNamePlaguesSicks = DiccionarioIdiomas.Instance.Traducir("NombrePlagasEnfermedades");
        string tagNameControllMalezas = DiccionarioIdiomas.Instance.Traducir("NombreControlMalezas");
        string tagNameTutorized = DiccionarioIdiomas.Instance.Traducir("TextTurorizadoP2");
        string tagNamePodaF = DiccionarioIdiomas.Instance.Traducir("TextPodaFormacionP2");
        string tagNamePodaS = DiccionarioIdiomas.Instance.Traducir("TextPodaSanitariaP2");
        string tagNameServiceGerminator = DiccionarioIdiomas.Instance.Traducir("NombreJornalGerminadero");
        string tagNamePlant = DiccionarioIdiomas.Instance.Traducir("NombrePlantas");
        string tagNamePlantUnidad = DiccionarioIdiomas.Instance.Traducir("NombrePlantasUnidad");
        
        switch (step)
        {
            case 0:
                if(CheckFields(fieldsFertilizer))
                {
                    PanelFertilizacion.Instance.ShowPanel(false);
                    float countAB1 = float.Parse(fieldsFertilizer[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countAB2 = float.Parse(fieldsFertilizer[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countAB3 = float.Parse(fieldsFertilizer[2].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countAB4 = float.Parse(fieldsFertilizer[3].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Fertilizacion1", tagNameFertilizer1, tagNamePackage, countAB1, myValuesProductsP2.precioAbono1);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Fertilizacion2", tagNameFertilizer2, tagNamePackage, countAB2, myValuesProductsP2.precioAbono2);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Fertilizacion3", tagNameFertilizer3, tagNamePackage, countAB3, myValuesProductsP2.precioAbono3);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Fertilizacion4", tagNameMaterialOrganic, tagNamePackage, countAB4, myValuesProductsP2.precioMateriaOrganica);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 1:
                if(CheckFields(fieldsPlagasEnfermedades))
                {
                    PanelPlagasEnfermedades.Instance.ShowPanel(false);
                    float countFe1 = float.Parse(fieldsPlagasEnfermedades[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countFe2 = float.Parse(fieldsPlagasEnfermedades[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countFe3 = float.Parse(fieldsPlagasEnfermedades[2].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Enfermedades1", tagNameInsecticide1, tagNameLiters, countFe1, myValuesProductsP2.precioInsecticida1);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Enfermedades2", tagNameInsecticide2, tagNameLiters, countFe2, myValuesProductsP2.precioInsecticida2);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Enfermedades3", tagNameFungicide, tagNameLiters, countFe2, myValuesProductsP2.precioFungicida1);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 2:
                if(CheckFields(fieldsMalezas))
                {
                    PanelControlMalezas.Instance.ShowPanel(false);
                    float countHerbi1 = float.Parse(fieldsMalezas[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countHerbi2 = float.Parse(fieldsMalezas[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Malezas1", tagNameHerbicide1, tagNameLiters, countHerbi1, myValuesProductsP2.precioHerbicida1);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Malezas2", tagNameHerbicide2, tagNameLiters, countHerbi2, myValuesProductsP2.precioHerbicida2);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 3:
                if(CheckFields(fieldsPodas))
                {
                    PanelPodas.Instance.ShowPanel(false);
                    float countTijeraGR = float.Parse(fieldsPodas[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float countTijeraPq = float.Parse(fieldsPodas[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Poda1", tagNameCutBig, tagNameTool, countTijeraGR, myValuesProductsP2.precioTijerasGr);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Poda2", tagNameCutLittle, tagNameTool, countTijeraPq, myValuesProductsP2.precioTijerasPq);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 4:
                if(CheckFields(fieldsTutorado))
                {
                    PanelTutorado.Instance.ShowPanel(false);
                    float countEstaca = float.Parse(fieldsTutorado[0].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    float cabuya = float.Parse(fieldsTutorado[1].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Tutorado1", tagNameStakes, tagNameTool, countEstaca, myValuesProductsP2.precioEstaca);
                    myInstantiateControll.InstanceProductInTablet("Cellar", "Tutorado2", tagNameRollCabuya, tagNameTool, cabuya, myValuesProductsP2.precioRolloCabuya);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                
                break;
            case 5:
                if(countAgua.text != "")
                {
                    float countAguaAux = float.Parse(countAgua.text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("HangarP2", "Agua", tagNameWater, tagNameMeasure, countAguaAux, myValuesProductsP2.precioAgua);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
            case 6:
                PanelRecursoHumano1Situacion2.Instance.ShowPanel(false);
                int countJor1 = int.Parse(dropsResorceHuman1[0].captionText.text);
                int countJor2 = int.Parse(dropsResorceHuman1[1].captionText.text);
                int countJor3 = int.Parse(dropsResorceHuman1[2].captionText.text);
                int countJor4 = int.Parse(dropsResorceHuman1[3].captionText.text);
                int countJor5 = int.Parse(dropsResorceHuman1[4].captionText.text);
                int countJor6 = int.Parse(dropsResorceHuman1[5].captionText.text);
                myInstantiateControll.InstanceProductInTablet("GreenHouseP2", "Fertilizantes", tagNameFertilization, tagNameJornal, countJor1, myValuesProductsP2.precioJornal);
                myInstantiateControll.InstanceProductInTablet("GreenHouseP2", "Incecticidas", tagNamePlaguesSicks, tagNameJornal, countJor2, myValuesProductsP2.precioJornal);
                myInstantiateControll.InstanceProductInTablet("GreenHouseP2", "Herbicidas", tagNameControllMalezas, tagNameJornal, countJor3, myValuesProductsP2.precioJornal);
                myInstantiateControll.InstanceProductInTablet("GreenHouseP2", "Tutorizado", tagNameTutorized, tagNameJornal, countJor4, myValuesProductsP2.precioJornal);
                myInstantiateControll.InstanceProductInTablet("GreenHouseP2", "PodaF", tagNamePodaF, tagNameJornal, countJor5, myValuesProductsP2.precioJornal);
                myInstantiateControll.InstanceProductInTablet("GreenHouseP2", "PodaS", tagNamePodaS, tagNameJornal, countJor6, myValuesProductsP2.precioJornal);
                break;
            case 7:
                PanelRecursoHumano2Situacion2.Instance.ShowPanel(false);
                int countJorGer = int.Parse(dropResourceHuman2.captionText.text);
                myInstantiateControll.InstanceProductInTablet("GerminatorP2", "Germinadero", tagNameServiceGerminator, tagNameJornal, countJorGer, myValuesProductsP2.precioJornal);
                break;
            case 8:
                if(countPlants.text != "")
                {
                    float countPl1 = float.Parse(countPlants.text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    myInstantiateControll.InstanceProductInTablet("GerminatorFijoP2", "cantPlantas", tagNamePlant, tagNamePlantUnidad, countPl1, myValuesProductsP2.precioPlanta);
                }
                else
                {
                    PanelMensajeInfo.Instance.ShowPanel();
                }
                break;
        }
    }

    public void ButtonRestar()
    {
        // SceneManager.UnloadSceneAsync("Practica2");
        // SceneManager.LoadScene("Practica2", LoadSceneMode.Additive);
        RestarPractice();
    }

    bool CheckFields(TMP_InputField[] fieldTest)
    {
        int test = 0;

        for(int i = 0; i < fieldTest.Length; i++)
        {
            if(fieldTest[i].text == "")
            {
                test = 1;
            }
        }
        return test > 0 ? false : true;
    }

    public void ScreenShootScenary()
    {
        StartCoroutine(ScreenShootScenaryCorutin());
    }

    IEnumerator ScreenShootScenaryCorutin()
    {
        cameraCaptureScenary.SetActive(true);
        yield return new WaitForEndOfFrame();
        cameraCaptureScenary.SetActive(false);
    }

    public void SaveConfirmation(TextMeshProUGUI textConfirmation)
    {
        StartCoroutine(EventSaveConfirmation(textConfirmation));
    }

    IEnumerator EventSaveConfirmation(TextMeshProUGUI textConfirmation)
    {
        textConfirmation.transform.gameObject.SetActive(true);
        yield return new WaitForSeconds(waitTimeConfirmation);
        textConfirmation.transform.gameObject.SetActive(false);
    }

    void RestarPractice()
    {
        /*
        for(int i = 0; i < containerTablet.transform.childCount; i++)
        {
            GameObject auxGO = containerTablet.transform.GetChild(i).gameObject;
            myRegistroProductoController = auxGO.GetComponent<RegistroProductoController>();
            
            float priceToRemove = 0;
            float lastPrice = float.Parse(auxGO.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text);
            float lastCount = float.Parse(auxGO.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text);
            priceToRemove = lastPrice * lastCount;
            myInstantiateControll.RemovePriceToCorrectZone(auxGO.tag, priceToRemove);
            myInstantiateControll.sumTotal.Remove(priceToRemove);
            myInstantiateControll.RealiceSumOfPrice();
            myRegistroProductoController.DeleteObjectInScene();
            Destroy(auxGO);
        }

        for(int j = 0; j < containerPanels.transform.childCount; j++)
        {
            containerPanels.transform.GetChild(j).gameObject.SetActive(false);
        }

        for(int k = 0; k < containerIncreases.Length; k++)
        {
            containerIncreases[k].SetActive(false);
        }

        PanelIngreseVolumenAgua.Instance.ShowPanel(false);
        PanelIngreseCantidadPlantulas.Instance.ShowPanel(false);
        myControllArrowBack.UpdateActualStateAndActiveZones(1);*/

        PlayerPrefs.SetInt("intentos", barraUsuario.GetAttempts());

        SceneLoader.Instance.LoadScene("Practica2", true, SceneManager.GetActiveScene());
    }
}