﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NSInterfaz;
using NSInterfazAvanzada;
using NSBoxMessage;
using NSTraduccionIdiomas;
using NSCalificacionSituacion;

public class RegisterDatesP2 : MonoBehaviour
{
    [Header("Referencia a inputField de registro de datos")]
    public TMP_InputField[] fieldRegisterDatesP2;

    [Header("Variable para calificación")]
    public float calificacion2;

    [Header("Referencia a scripts")]
    [SerializeField] private InstantiateController myInstanceController;
    [SerializeField] private CalculosCantidadProductosP2 myCalculoCantidadProducts;

    [Header("Referencia a objetos en escena")]
    [SerializeField] private GameObject[] xInFields;
    
    [Header("Porcentaje de error")]
    [SerializeField] private float valueError;

    [Header("variables para intentos")]
    [SerializeField] private TextMeshProUGUI AttemptsText;
    [SerializeField] private SOSessionData soSessionData;
    private float countAttempts = 1;

    private float sumTotalDatesP2;
    private float sumCellar;
    private float sumHangarP2;
    private float sumGreenHouseP2;
    [SerializeField] private List<float> listValuesToCompare = new List<float>();
    private bool canContinius;

    public void ButtonCheckDates()
    {
        SumToFields();
        SumPriceOfProductsInBuilds();
        CheckValuesInRegisterAndTable();
    }

    void SumToFields()
    {
        sumTotalDatesP2 = 0;
        for(int i = 0; i < fieldRegisterDatesP2.Length - 1; i ++)
        {
            if(fieldRegisterDatesP2[i].text != "")
            {
                sumTotalDatesP2 += float.Parse(fieldRegisterDatesP2[i].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            }
        }
    }

    void SumPriceOfProductsInBuilds()
    {
        listValuesToCompare.Clear();
        sumHangarP2 = 0;
        sumGreenHouseP2 = 0;
        sumCellar = 0;

        for(int i = 0; i < myInstanceController.listCellar.Count; i++)
        {
            sumCellar += myInstanceController.listCellar[i];
        }
        listValuesToCompare.Add(sumCellar);

        for(int i = 0; i < myInstanceController.listGreenHouseP2.Count; i++)
        {
            sumGreenHouseP2 += myInstanceController.listGreenHouseP2[i];
        }
        listValuesToCompare.Add(sumGreenHouseP2);

        for(int i = 0; i < myInstanceController.listHangarP2.Count; i++)
        {
            sumHangarP2 += myInstanceController.listHangarP2[i];
        }
        listValuesToCompare.Add(sumHangarP2);
        listValuesToCompare.Add(sumTotalDatesP2);
    }

    void CheckValuesInRegisterAndTable()
    {
        int countErrors = 0;
        calificacion2 = 0;
        for(int i = 0; i < fieldRegisterDatesP2.Length; i++)
        {
            if(fieldRegisterDatesP2[i].text != "")
            {
                float valueUser = float.Parse(fieldRegisterDatesP2[i].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                float valueCompare = listValuesToCompare[i];
                if(valueUser >= (valueCompare / valueError) && valueUser <= (valueCompare * valueError))
                {
                    xInFields[i].SetActive(false);
                    calificacion2 += 1f / fieldRegisterDatesP2.Length;
                }
                else
                {
                    xInFields[i].SetActive(true);
                    countErrors++;
                    Debug.Log("usuario: " + valueUser + " compare: " + valueCompare);
                }
            }
            else 
            {
                xInFields[i].SetActive(true);
                countErrors++;
            }
        }

        if(countErrors > 0)
        {
            PanelMensajeIncorrecto.Instance.ShowPanel();
            countAttemptsMetod();
        }
        else
        {
            BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeDatosIngresadosCorrectos"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), null, null);
            canContinius = true;
        }
    }

    public void ButtonReport()
    {
        if(canContinius)
        {
            PanelRegistroDatos.Instance.ShowPanel(false);
            PanelInterfazEvaluacion.Instance.ShowPanel();
        }
        else
        {
            BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeDatosIngresadosIncorrectos"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), ButtonAceptar, null);
        }
    }

    void ButtonAceptar()
    {
        PanelRegistroDatos.Instance.ShowPanel(false);
        PanelInterfazEvaluacion.Instance.ShowPanel();
        countAttemptsMetod();
    }

    public void countAttemptsMetod()
    {
        countAttempts += 1;
        AttemptsText.text = countAttempts.ToString();
        soSessionData.quantityAttempts = (byte) countAttempts;
    }
}
