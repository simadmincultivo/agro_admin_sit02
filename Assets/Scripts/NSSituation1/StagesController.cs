﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using NSTraduccionIdiomas;
using NSInterfaz;

namespace NSActiveZone
{
    public class StagesController : AbstractObject2D
    {
        void Start()
        {
            //DiccionarioIdiomas.Instance.Traducir("TextoInformacionDeAfuera");
            ActivateAllActiveZonesOfActualStage(true);
            //PanelInterfazBienvenida.Instance.ShowPanel();
            SceneManager.sceneLoaded += OnSceneLoaded;//se suscribe un metodo para saber cuando se carga una nueva scena y de que manera
        }

        public override void OnMoving()
        {
            
        }

        public override void OnTab()
        {
            
        }

        /// <summary>
        /// cuando una nueva scena termina de cargarse, se ejecuta este metodo
        /// </summary>
        /// <param name="argScene">Scena que se cargo, se puede usar para obtener el nombre y otras cosas</param>
        /// <param name="argLoadSceneMode">De que modo se cargo la scena? sirve para saber si se cargo additivamente o borrando todas las anteriores</param>
        private void OnSceneLoaded(Scene argScene, LoadSceneMode argLoadSceneMode)
        {
            var tmpSceneToLoad = SceneManager.GetSceneByName(argScene.name);
            SceneManager.SetActiveScene(tmpSceneToLoad);//coloco la scena como la principal, para saber cual fue la ultima que cargue
        }

        public override void OnTabOverActiveZone(string argNameActiveZone)
        {
            if(argNameActiveZone.Equals("ZoneSituacion1"))
            {
                SceneManager.LoadScene("Practica1", LoadSceneMode.Additive);            
            }
            else if(argNameActiveZone.Equals("ZoneSituacion2"))
            {
                SceneManager.LoadScene("Practica2", LoadSceneMode.Additive);
            }
            else if(argNameActiveZone.Equals("ZoneSituacion3"))
            {
                SceneManager.LoadScene("Practica3", LoadSceneMode.Additive);
            }
            else if(argNameActiveZone.Equals("ZoneSituacion4"))
            {
                SceneManager.LoadScene("Practica4", LoadSceneMode.Additive);
            }

           // SceneManager.UnloadSceneAsync("Menu");
        }

        protected override void OnReleaseObjectOverActiveZone(string argNameActiveZone)
        {
            
        }

        protected override void OnReleaseObjectOverNothing()
        {
            
        }
    }
}
