﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NSInterfaz;
using NSBoxMessage;
using NSInterfazAvanzada;
using NSTraduccionIdiomas;
using NSCalificacionSituacion;

public class RegisterOfDates : MonoBehaviour
{
    [Header("Variables para calificación")]
    public float calification1;
    public float valorParaRealizacion;

    [Header("Referencias a Script")]
    [SerializeField] private InstantiateController myInstanciateController;
    
    [Header("Referencia a objetos Ui en escena")]
    [SerializeField] private TMP_InputField[] fieldsRegisterDates;
    [SerializeField] private GameObject[] xInFields;
    [SerializeField] private TMP_InputField fieldFulfilled;    

    [Header("Porcentaje de error")]
    [SerializeField] private float valueError;

    private float sumGreenHouse = 0;
    private float sumHangar = 0;
    private float sumGerminator = 0;
    private List<float> listValuesToCompare = new List<float>();
    private bool canContinius = false;
    private List<float> listValuesToRealization = new List<float>();
    private float valueMinRealization = 18600;//le restamos 100 al valor original, para que se agregue el original a la lista
    private float valueMaxRealization = 25300;

    [Header("Variables para intentos")]
    [SerializeField] TextMeshProUGUI attemptsText;
    [SerializeField] SOSessionData soSessionData;
    private int countAttempts = 1;

    void Awake()
    {
        for(int i = 0; i < 67; i++)
        {
            valueMinRealization += 100;

            if(valueMinRealization <= valueMaxRealization)
            {
                listValuesToRealization.Add(valueMinRealization);
            }
        }
        int selectValue = Random.Range(0, 67);
        valorParaRealizacion = listValuesToRealization[selectValue];
    }

    public void ButtonCheck()
    {        
        SumPriceOfProductsInBuilds();
        CheckDatesOfRegisterDates();
    }

    void SumPriceOfProductsInBuilds()
    {
        listValuesToCompare.Clear();
        sumGreenHouse = 0;
        sumHangar = 0;
        sumGerminator = 0;
        
        for(int i = 0; i < myInstanciateController.listGreenHouse.Count; i++)
        {
            sumGreenHouse += myInstanciateController.listGreenHouse[i]; 
        }
        listValuesToCompare.Add(sumGreenHouse);

        for(int i = 0; i < myInstanciateController.listHangar.Count; i++)
        {
            sumHangar += myInstanciateController.listHangar[i];
        }
        listValuesToCompare.Add(sumHangar);

        for(int i = 0; i < myInstanciateController.listGerminator.Count; i++)
        {
            sumGerminator += myInstanciateController.listGerminator[i];
        }
        listValuesToCompare.Add(sumGerminator);
        listValuesToCompare.Add(myInstanciateController.countToView);
    }
    void CheckDatesOfRegisterDates()
    {  
        int countErrors = 0;
        calification1 = 0;
        for(int i = 0; i < (fieldsRegisterDates.Length); i++)
        {
            if(fieldsRegisterDates[i].text != "")
            {
                float valueCompare = listValuesToCompare[i];
                float valueUser = float.Parse(fieldsRegisterDates[i].text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                if(valueUser >= (valueCompare / valueError) && valueUser <= (valueCompare * valueError))
                {
                    xInFields[i].SetActive(false);
                    calification1 += (1f / (fieldsRegisterDates.Length + 1));
                }
                else
                {
                    xInFields[i].SetActive(true);
                    countErrors++;
                }
            }
            else 
            {
                xInFields[i].SetActive(true);
                countErrors++;
            }
        }        

        if(countErrors > 0)
        {
            PanelMensajeIncorrecto.Instance.ShowPanel();
            canContinius = false;
            
            if(myInstanciateController.countToView <= (valorParaRealizacion * valueError))
            {
                xInFields[xInFields.Length - 1].SetActive(false);
                fieldFulfilled.text = DiccionarioIdiomas.Instance.Traducir("TextSIP1");
            }
            else
            {
                xInFields[xInFields.Length - 1].SetActive(true);
                fieldFulfilled.text = DiccionarioIdiomas.Instance.Traducir("TextNOP1");
            }
            countAttemptsMetod();
        }
        else 
        {
            canContinius = true;

            if(myInstanciateController.countToView <= (valorParaRealizacion * valueError))
            {
                xInFields[xInFields.Length - 1].SetActive(false);
                fieldFulfilled.text = DiccionarioIdiomas.Instance.Traducir("TextSIP1");
                calification1 += (1f / (fieldsRegisterDates.Length + 1));
                BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeDatosIngresadosCorrectos"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), null, null);
            }
            else
            {
                PanelMensajeIncorrecto.Instance.ShowPanel();
                xInFields[xInFields.Length - 1].SetActive(true);
                fieldFulfilled.text = DiccionarioIdiomas.Instance.Traducir("TextNOP1");
            }
        }
    }

    public void ButtonReport()
    {
        if(canContinius)
        {
            PanelRegistroDatos.Instance.ShowPanel(false);
            PanelInterfazEvaluacion.Instance.ShowPanel();
        }
        else
        {
            BoxMessageManager.Instance.CreateBoxMessageDecision(DiccionarioIdiomas.Instance.Traducir("mensajeDatosIngresadosIncorrectos"), DiccionarioIdiomas.Instance.Traducir("TextCancelar"), DiccionarioIdiomas.Instance.Traducir("TextAceptarMayusculas"), ButtonAceptar, null);
        }
    }

    void ButtonAceptar()
    {
        PanelRegistroDatos.Instance.ShowPanel(false);
        PanelInterfazEvaluacion.Instance.ShowPanel();
        countAttemptsMetod();
    }

    public void countAttemptsMetod()
    {
        countAttempts += 1;
        attemptsText.text = countAttempts.ToString();
        soSessionData.quantityAttempts = (byte) countAttempts;
    }
}
