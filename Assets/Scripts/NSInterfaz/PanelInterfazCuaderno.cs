﻿using NSAvancedUI;
using TMPro;
using NSTraduccionIdiomas;
using UnityEngine;

namespace NSInterfaz 
{
	public class PanelInterfazCuaderno : AbstractSingletonPanelUIAnimation<PanelInterfazCuaderno>
    {
        [SerializeField] private TextMeshProUGUI textPlacerHolder;

        //Código Luis
        public void RefreshTextPlaceHolder()
        {
            textPlacerHolder.text = DiccionarioIdiomas.Instance.Traducir("TextEscribeAqui");
        }
        //Fin Código Luis
	    
    }
}