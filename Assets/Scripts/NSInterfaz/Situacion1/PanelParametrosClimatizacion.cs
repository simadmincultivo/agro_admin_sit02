﻿
using NSAvancedUI;
using UnityEngine;
using TMPro;
using NSTraduccionIdiomas;

namespace NSInterfaz
{
    public class PanelParametrosClimatizacion : AbstractSingletonPanelUIAnimation<PanelParametrosClimatizacion>
    {
        #region members
        [Header("Referencia de DropDown climatización")]
        [SerializeField] private TMP_Dropdown dropClimatization;
        

        #endregion

        #region MonoBehaviour

        #endregion

        #region private methods

        private void Awake()
        {
            dropClimatization.options[0].text = DiccionarioIdiomas.Instance.Traducir("TextRefrigeracion");
            dropClimatization.options[1].text = DiccionarioIdiomas.Instance.Traducir("TextCalefaccion");
        }

        #endregion

        #region public methods

        #endregion
    }
}