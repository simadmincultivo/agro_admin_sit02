﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSCuaderno;

public class initCuaderno : MonoBehaviour
{
    public Cuaderno objetoCuaderno;

    void Start()
    {
        objetoCuaderno.InitCuaderno();
        objetoCuaderno.mtdPasarInfoAlPdf();
    }
    private void OnEnable()
    {
        
    }

}
